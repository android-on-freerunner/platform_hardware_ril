/* //device/system/reference-ril/freerunner-ril.c
**
** Copyright 2006, The Android Open Source Project
**
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

#include <telephony/ril.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <alloca.h>
#include "atchannel.h"
#include "at_tok.h"
#include "misc.h"
#include <getopt.h>
#include <sys/socket.h>
#include <cutils/sockets.h>
#include <cutils/properties.h>
#include <termios.h>
#include <time.h>
#include <signal.h>

#define LOG_TAG "RIL"
#include <utils/Log.h>

struct ATChannels *ATch_primary;
struct ATChannels *ATch_gprs;
static int gprs_status = 0;

#define PROPERTY_GPRS_CHANNEL	"vchanneld.gprs"
#define PROPERTY_DEEP_SLEEP_ENABLE "freerunner.deep.sleep.enable"

#define CALYPSO_CPATH	"/sys/bus/platform/devices/neo1973-pm-gsm.0/flowcontrolled"

/* pathname returned from RIL_REQUEST_SETUP_DATA_CALL */
#define PPP_TTY_PATH "/dev/pts/1"
#define PPP_OPTION_FILE "/data/ppp/options.gprs"

char gprs_device[PROPERTY_VALUE_MAX];

#ifdef USE_TI_COMMANDS

// Enable a workaround
// 1) Make incoming call, do not answer
// 2) Hangup remote end
// Expected: call should disappear from CLCC line
// Actual: Call shows as "ACTIVE" before disappearing
#define WORKAROUND_ERRONEOUS_ANSWER 1

// Some varients of the TI stack do not support the +CGEV unsolicited
// response. However, they seem to send an unsolicited +CME ERROR: 150
#define WORKAROUND_FAKE_CGEV 1
#endif

#define FACILITY_LOCK_REQUEST "2"

enum sms_init_status {
    CSTAT_SMS = 1,
    CSTAT_PHB = 2
};

typedef enum {
    SIM_ABSENT = 0,
    SIM_NOT_READY = 1,
    SIM_READY = 2, /* SIM_READY means the radio state is RADIO_STATE_SIM_READY */
    SIM_PIN = 3,
    SIM_PUK = 4,
    SIM_NETWORK_PERSONALIZATION = 5
} SIM_Status;

static void onRequest (int request, void *data, size_t datalen, RIL_Token t);
static RIL_RadioState currentState();
static int onSupports (int requestCode);
static void onCancel (RIL_Token t);
static const char *getVersion();
static int isRadioOn();
static SIM_Status getSIMStatus();
static int getCardStatus(RIL_CardStatus **pp_card_status);
static void freeCardStatus(RIL_CardStatus *p_card_status);
static void onPDPContextListChanged(void *param);

extern const char * requestToString(int request);

/*** Static Variables ***/
static const RIL_RadioFunctions s_callbacks = {
    RIL_VERSION,
    onRequest,
    currentState,
    onSupports,
    onCancel,
    getVersion
};

#ifdef RIL_SHLIB
static const struct RIL_Env *s_rilenv;

#define RIL_onRequestComplete(t, e, response, responselen) s_rilenv->OnRequestComplete(t,e, response, responselen)
#define RIL_onUnsolicitedResponse(a,b,c) s_rilenv->OnUnsolicitedResponse(a,b,c)
#define RIL_requestTimedCallback(a,b,c) s_rilenv->RequestTimedCallback(a,b,c)
#endif

static RIL_RadioState sState = RADIO_STATE_UNAVAILABLE;

static pthread_mutex_t s_state_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t s_state_cond = PTHREAD_COND_INITIALIZER;

static int s_port = -1;
static const char * s_device_path = NULL;
static int          s_device_socket = 0;

/* SIM is ready. This is a CALYPSO workaround*/
static enum sms_init_status simReady;
static int screenStatus = 0;

/* trigger change to this with s_state_cond */
static int s_closed = 0;

static int sFD;     /* file desc of AT channel */

static const struct timeval TIMEVAL_SIMPOLL = {1,0};
static const struct timeval TIMEVAL_CALLSTATEPOLL = {0,500000};
static const struct timeval TIMEVAL_0 = {0,0};

// Mapping from ETSI GSM 03.38 7-bit default alphabet to Unicode
// from http://unicode.org/Public/MAPPINGS/ETSI/GSM0338.TXT
static const unsigned long gsm_to_unicode[] =
        {
            0x0040,  // COMMERCIAL AT
            0x00A3,  // POUND SIGN
            0x0024,  // DOLLAR SIGN
            0x00A5,  // YEN SIGN
            0x00E8,  // LATIN SMALL LETTER E WITH GRAVE
            0x00E9,  // LATIN SMALL LETTER E WITH ACUTE
            0x00F9,  // LATIN SMALL LETTER U WITH GRAVE
            0x00EC,  // LATIN SMALL LETTER I WITH GRAVE
            0x00F2,  // LATIN SMALL LETTER O WITH GRAVE
            0x00E7,  // LATIN SMALL LETTER C WITH CEDILLA
            0x000A,  // LINE FEED
            0x00D8,  // LATIN CAPITAL LETTER O WITH STROKE
            0x00F8,  // LATIN SMALL LETTER O WITH STROKE
            0x000D,  // CARRIAGE RETURN
            0x00C5,  // LATIN CAPITAL LETTER A WITH RING ABOVE
            0x00E5,  // LATIN SMALL LETTER A WITH RING ABOVE
            0x0394,  // GREEK CAPITAL LETTER DELTA
            0x005F,  // LOW LINE
            0x03A6,  // GREEK CAPITAL LETTER PHI
            0x0393,  // GREEK CAPITAL LETTER GAMMA
            0x039B,  // GREEK CAPITAL LETTER LAMDA
            0x03A9,  // GREEK CAPITAL LETTER OMEGA
            0x03A0,  // GREEK CAPITAL LETTER PI
            0x03A8,  // GREEK CAPITAL LETTER PSI
            0x03A3,  // GREEK CAPITAL LETTER SIGMA
            0x0398,  // GREEK CAPITAL LETTER THETA
            0x039E,  // GREEK CAPITAL LETTER XI
            0x00A0,  // ESCAPE TO EXTENSION TABLE (or displayed as NBSP, see note above)
            // 0x1B0A	0x000C	//	FORM FEED
            // 0x1B14	0x005E	//	CIRCUMFLEX ACCENT
            // 0x1B28	0x007B	//	LEFT CURLY BRACKET
            // 0x1B29	0x007D	//	RIGHT CURLY BRACKET
            // 0x1B2F	0x005C	//	REVERSE SOLIDUS
            // 0x1B3C	0x005B	//	LEFT SQUARE BRACKET
            // 0x1B3D	0x007E	//	TILDE
            // 0x1B3E	0x005D	//	RIGHT SQUARE BRACKET
            // 0x1B40	0x007C	//	VERTICAL LINE
            // 0x1B65	0x20AC	//	EURO SIGN
            0x00C6,  // LATIN CAPITAL LETTER AE
            0x00E6,  // LATIN SMALL LETTER AE
            0x00DF,  // LATIN SMALL LETTER SHARP S (German)
            0x00C9,  // LATIN CAPITAL LETTER E WITH ACUTE
            0x0020,  // SPACE
            0x0021,  // EXCLAMATION MARK
            0x0022,  // QUOTATION MARK
            0x0023,  // NUMBER SIGN
            0x00A4,  // CURRENCY SIGN
            0x0025,  // PERCENT SIGN
            0x0026,  // AMPERSAND
            0x0027,  // APOSTROPHE
            0x0028,  // LEFT PARENTHESIS
            0x0029,  // RIGHT PARENTHESIS
            0x002A,  // ASTERISK
            0x002B,  // PLUS SIGN
            0x002C,  // COMMA
            0x002D,  // HYPHEN-MINUS
            0x002E,  // FULL STOP
            0x002F,  // SOLIDUS
            0x0030,  // DIGIT ZERO
            0x0031,  // DIGIT ONE
            0x0032,  // DIGIT TWO
            0x0033,  // DIGIT THREE
            0x0034,  // DIGIT FOUR
            0x0035,  // DIGIT FIVE
            0x0036,  // DIGIT SIX
            0x0037,  // DIGIT SEVEN
            0x0038,  // DIGIT EIGHT
            0x0039,  // DIGIT NINE
            0x003A,  // COLON
            0x003B,  // SEMICOLON
            0x003C,  // LESS-THAN SIGN
            0x003D,  // EQUALS SIGN
            0x003E,  // GREATER-THAN SIGN
            0x003F,  // QUESTION MARK
            0x00A1,  // INVERTED EXCLAMATION MARK
            0x0041,  // LATIN CAPITAL LETTER A
            0x0042,  // LATIN CAPITAL LETTER B
            0x0043,  // LATIN CAPITAL LETTER C
            0x0044,  // LATIN CAPITAL LETTER D
            0x0045,  // LATIN CAPITAL LETTER E
            0x0046,  // LATIN CAPITAL LETTER F
            0x0047,  // LATIN CAPITAL LETTER G
            0x0048,  // LATIN CAPITAL LETTER H
            0x0049,  // LATIN CAPITAL LETTER I
            0x004A,  // LATIN CAPITAL LETTER J
            0x004B,  // LATIN CAPITAL LETTER K
            0x004C,  // LATIN CAPITAL LETTER L
            0x004D,  // LATIN CAPITAL LETTER M
            0x004E,  // LATIN CAPITAL LETTER N
            0x004F,  // LATIN CAPITAL LETTER O
            0x0050,  // LATIN CAPITAL LETTER P
            0x0051,  // LATIN CAPITAL LETTER Q
            0x0052,  // LATIN CAPITAL LETTER R
            0x0053,  // LATIN CAPITAL LETTER S
            0x0054,  // LATIN CAPITAL LETTER T
            0x0055,  // LATIN CAPITAL LETTER U
            0x0056,  // LATIN CAPITAL LETTER V
            0x0057,  // LATIN CAPITAL LETTER W
            0x0058,  // LATIN CAPITAL LETTER X
            0x0059,  // LATIN CAPITAL LETTER Y
            0x005A,  // LATIN CAPITAL LETTER Z
            0x00C4,  // LATIN CAPITAL LETTER A WITH DIAERESIS
            0x00D6,  // LATIN CAPITAL LETTER O WITH DIAERESIS
            0x00D1,  // LATIN CAPITAL LETTER N WITH TILDE
            0x00DC,  // LATIN CAPITAL LETTER U WITH DIAERESIS
            0x00A7,  // SECTION SIGN
            0x00BF,  // INVERTED QUESTION MARK
            0x0061,  // LATIN SMALL LETTER A
            0x0062,  // LATIN SMALL LETTER B
            0x0063,  // LATIN SMALL LETTER C
            0x0064,  // LATIN SMALL LETTER D
            0x0065,  // LATIN SMALL LETTER E
            0x0066,  // LATIN SMALL LETTER F
            0x0067,  // LATIN SMALL LETTER G
            0x0068,  // LATIN SMALL LETTER H
            0x0069,  // LATIN SMALL LETTER I
            0x006A,  // LATIN SMALL LETTER J
            0x006B,  // LATIN SMALL LETTER K
            0x006C,  // LATIN SMALL LETTER L
            0x006D,  // LATIN SMALL LETTER M
            0x006E,  // LATIN SMALL LETTER N
            0x006F,  // LATIN SMALL LETTER O
            0x0070,  // LATIN SMALL LETTER P
            0x0071,  // LATIN SMALL LETTER Q
            0x0072,  // LATIN SMALL LETTER R
            0x0073,  // LATIN SMALL LETTER S
            0x0074,  // LATIN SMALL LETTER T
            0x0075,  // LATIN SMALL LETTER U
            0x0076,  // LATIN SMALL LETTER V
            0x0077,  // LATIN SMALL LETTER W
            0x0078,  // LATIN SMALL LETTER X
            0x0079,  // LATIN SMALL LETTER Y
            0x007A,  // LATIN SMALL LETTER Z
            0x00E4,  // LATIN SMALL LETTER A WITH DIAERESIS
            0x00F6,  // LATIN SMALL LETTER O WITH DIAERESIS
            0x00F1,  // LATIN SMALL LETTER N WITH TILDE
            0x00FC,  // LATIN SMALL LETTER U WITH DIAERESIS
            0x00E0,  // LATIN SMALL LETTER A WITH GRAVE
        };

static unsigned long
extension_char(char c)
{
    switch (c) {
    case 0x0A:
        return 0x000C;  // FORM FEED
    case 0x14:
        return 0x005E;  // CIRCUMFLEX ACCENT
    case 0x28:
        return 0x007B;  // LEFT CURLY BRACKET
    case 0x29:
        return 0x007D;  // RIGHT CURLY BRACKET
    case 0x2f:
        return 0x005C;  // REVERSE SOLIDUS
    case 0x3c:
        return 0x005B;  // LEFT SQUARE BRACKET
    case 0x3d:
        return 0x007E;  // TILDE
    case 0x3e:
        return 0x005D;  // RIGHT SQUARE BRACKET
    case 0x40:
        return 0x007C;  // VERTICAL LINE
    case 0x65:
        return 0x20AC;  // EURO SIGN
    default:
        return 0x0020;  // SPACE
    }
}
static char *
utf8cat(char * buf, unsigned long unicode_char) {
    char utf8[5];
    int pos = 0;
    if (unicode_char >= 0x10000) {
        utf8[pos++] = (char)(0xf0 | (unicode_char >> 18));
        utf8[pos++] = (char)(0x80 | (0x3f & (unicode_char >> 12)));
        utf8[pos++] = (char)(0x80 | (0x3f & (unicode_char >> 6)));
        utf8[pos++] = (char)(0x80 | (0x3f & unicode_char));
        utf8[pos++] = '\0';
    } else if (unicode_char >= 0x0800) {
        utf8[pos++] = (char)(0xe0 | (unicode_char >> 12));
        utf8[pos++] = (char)(0x80 | (0x3f & (unicode_char >> 6)));
        utf8[pos++] = (char)(0x80 | (0x3f & unicode_char));
        utf8[pos++] = '\0';
    } else if (unicode_char >= 0x0080) {
        utf8[pos++] = (char)(0xc0 | (unicode_char >> 6));
        utf8[pos++] = (char)(0x80 | (0x3f & unicode_char));
        utf8[pos++] = '\0';
    } else { // U+0000-U+007F
        utf8[pos++] = (char)unicode_char;
        utf8[pos++] = '\0';
    }
    return strcat(buf, utf8);
}

static void gsm_set(char *path, int on)
{
    int fd, ret;
    char cmd;

    do {
            fd = open(path, O_RDWR);
    } while (fd < 0 && errno == EINTR);

    if (fd < 0) {
        LOGE("could not open GSM");
        return;
    }

    if (on)
        cmd = '1';
    else
        cmd = '0';

    do {
        ret = write(fd, &cmd, 1);
    } while (ret < 0 && errno == EINTR);

    close(fd);
}

#ifdef FAKE_HAVE_TI_CALYPSO
#define MILLISEC_TO_MINUTE (60 * 1000)
struct Network_info {
    int status;
    int cid;
    int n_registration;
    struct timeval last_time;
    int force_check;
    int deep_mode;
};

struct Network_info NetInfo;

static void deepModeEnable(void *param)
{
   /* Try to reanable deep mode after half an hour */
   at_send_command(ATch_primary, "AT%SLEEP=4", NULL);
}

static void getNow(struct timeval * tv)
{
#ifdef HAVE_POSIX_CLOCKS
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    tv->tv_sec = ts.tv_sec;
    tv->tv_usec = ts.tv_nsec/1000;
#else
    gettimeofday(tv, NULL);
#endif
}

static int diff_timeval_to_ms(struct timeval *end, struct timeval *start)
{
    return ((end->tv_sec - start->tv_sec) * 1000) + ((end->tv_usec - start->tv_usec) / 1000);
}

/* This save the internal network status */
void internalNetworkStatusRegistration(int status, int cid) {
    struct timeval event_time;

    getNow(&event_time);

    NetInfo.status = status;

    if (!NetInfo.deep_mode)
	return;

    /* Status five is a registration. So if the network was
     * disconnected and we try to reconnect to the new one
     * we must consider this registration and count it */
    if (status == 5 && cid != -1 && NetInfo.cid == -1) {
        int diff;
        NetInfo.cid = cid;

        LOGD("event_time sec %ld last_time %ld", event_time.tv_sec,
              NetInfo.last_time.tv_sec);

        diff = diff_timeval_to_ms(&event_time, &NetInfo.last_time);
        /* The user can change date/time in the middle. I think that the
         * system will be report a new one date/time and we reset the
         * info every 8 minutes */
        if (diff <= 0 || diff >= (8 * MILLISEC_TO_MINUTE)) {
                NetInfo.n_registration = 0;
                getNow(&NetInfo.last_time);
                NetInfo.deep_mode = 1;
                goto out;
        }
        NetInfo.n_registration++;
        if (NetInfo.n_registration > 5 && diff <= (2 * MILLISEC_TO_MINUTE)) {
            /* We have a lot of reregistration change the deep_mode */
            NetInfo.deep_mode = 0;
            goto out;
        } else if (NetInfo.n_registration > 5) {
            LOGD("Start again n_registration %d, diff %d",
                 NetInfo.n_registration, diff);
            /* Start again #1024 */
            NetInfo.n_registration = 0;
            NetInfo.deep_mode = 1;
            getNow(&NetInfo.last_time);
            goto out;
        }
    } else if (cid == -1 && NetInfo.force_check) {
        /* This is a disconection after CSQ drops to 99 */
        NetInfo.deep_mode = 0;
        NetInfo.cid = -1;
        getNow(&NetInfo.last_time);
        NetInfo.force_check = 0;
    } else if (status == 0) {
        NetInfo.cid = -1;
    }
out:
   NetInfo.force_check = 0;
   /* Deep mode depend on BUG #1024 */
   if (!NetInfo.deep_mode)
       at_send_command(ATch_primary, "AT%SLEEP=2", NULL);
}
#endif

#ifdef WORKAROUND_ERRONEOUS_ANSWER
// Max number of times we'll try to repoll when we think
// we have a AT+CLCC race condition
#define REPOLL_CALLS_COUNT_MAX 4

// Line index that was incoming or waiting at last poll, or -1 for none
static int s_incomingOrWaitingLine = -1;
// Number of times we've asked for a repoll of AT+CLCC
static int s_repollCallsCount = 0;
// Should we expect a call to be answered in the next CLCC?
static int s_expectAnswer = 0;
#endif /* WORKAROUND_ERRONEOUS_ANSWER */

static void pollSIMState (void *param);
static void setRadioState(RIL_RadioState newState);

#define WRITE_PPP_OPTION(option) write(fd, option, strlen(option))

static int deactivatePDPConnection()
{
    int err = 0;

    if (gprs_status) {
        LOGD("Try to deactivated modem ...");
        tcflush(ATch_gprs->s_fd, TCIOFLUSH);
#ifdef HAVE_TI_CALYPSO
        write(ATch_gprs->s_fd, "\x1a", 1);
        usleep(250000);
#endif
        err = at_deactivate_modem(ATch_gprs);
        if (err == 0)
            gprs_status = 0;
    }
    tcflush(ATch_gprs->s_fd, TCIOFLUSH);

    return err;
}

static void writePppOptions(int channel, const char *tty_path,
                            const char *username, const char *password)
{
    char *line;
    int fd = creat (PPP_OPTION_FILE, 0644);
    if (fd < 0) {
        LOGE ("Error creating PPP options file: " PPP_OPTION_FILE ".\n");
        return;
    }
    asprintf(&line, "%s\n", tty_path);
    WRITE_PPP_OPTION(line);
    free(line);

    WRITE_PPP_OPTION("crtscts\n");

    if (username) {
        asprintf(&line, "user %s\n", username);
        WRITE_PPP_OPTION(line);
        free(line);
    }

    if (password) {
        asprintf(&line, "password %s\n", password);
        WRITE_PPP_OPTION(line);
        free(line);
    }

    asprintf(&line, "unit %d\n", channel);
    WRITE_PPP_OPTION(line);
    free(line);

    WRITE_PPP_OPTION("noauth\n");
    WRITE_PPP_OPTION("nodetach\n");
    WRITE_PPP_OPTION("default-asyncmap\n");
    WRITE_PPP_OPTION("ipcp-accept-local\n");
    WRITE_PPP_OPTION("ipcp-accept-remote\n");
    WRITE_PPP_OPTION("usepeerdns\n");
    WRITE_PPP_OPTION("defaultroute\n");

    close(fd);
}

void set_sleep_mode()
{
    char deep_sleep_enable[PROPERTY_VALUE_MAX];
    property_get(PROPERTY_DEEP_SLEEP_ENABLE, deep_sleep_enable, "0");
    if (*deep_sleep_enable == '1')
        at_send_command(ATch_primary, "AT%SLEEP=4", NULL);
    else
        at_send_command(ATch_primary, "AT%SLEEP=2", NULL);
}

static int isSMSInit()
{
    return simReady == (CSTAT_SMS | CSTAT_PHB) ? 1 : 0;
}

static int clccStateToRILState(int state, RIL_CallState *p_state)

{
    switch(state) {
        case 0: *p_state = RIL_CALL_ACTIVE;   return 0;
        case 1: *p_state = RIL_CALL_HOLDING;  return 0;
        case 2: *p_state = RIL_CALL_DIALING;  return 0;
        case 3: *p_state = RIL_CALL_ALERTING; return 0;
        case 4: *p_state = RIL_CALL_INCOMING; return 0;
        case 5: *p_state = RIL_CALL_WAITING;  return 0;
        default: return -1;
    }
}

/**
 * Note: directly modified line and has *p_call point directly into
 * modified line
 */
static int callFromCLCCLine(char *line, RIL_Call *p_call)
{
        //+CLCC: 1,0,2,0,0,\"+18005551212\",145
        //     index,isMT,state,mode,isMpty(,number,TOA)?

    int err;
    int state;
    int mode;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(p_call->index));
    if (err < 0) goto error;

    err = at_tok_nextbool(&line, &(p_call->isMT));
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &state);
    if (err < 0) goto error;

    err = clccStateToRILState(state, &(p_call->state));
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &mode);
    if (err < 0) goto error;

    p_call->isVoice = (mode == 0);

    err = at_tok_nextbool(&line, &(p_call->isMpty));
    if (err < 0) goto error;

    if (at_tok_hasmore(&line)) {
        err = at_tok_nextstr(&line, &(p_call->number));

        /* tolerate null here */
        if (err < 0) return 0;

        // Some lame implementations return strings
        // like "NOT AVAILABLE" in the CLCC line
        if (p_call->number != NULL
            && 0 == strspn(p_call->number, "+0123456789")
        ) {
            p_call->number = NULL;
        }

        err = at_tok_nextint(&line, &p_call->toa);
        if (err < 0) goto error;
    }

    return 0;

error:
    LOGE("invalid CLCC line\n");
    return -1;
}

static int forwardFromCCFCLine(char *line, RIL_CallForwardInfo *p_forward)
{
    int err;
    int state;
    int mode;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(p_forward->status));
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(p_forward->serviceClass));
    if (err < 0) goto error;

    if (at_tok_hasmore(&line)) {
        err = at_tok_nextstr(&line, &(p_forward->number));

        /* tolerate null here */
        if (err < 0) return 0;

        if (p_forward->number != NULL
            && 0 == strspn(p_forward->number, "+0123456789")
           ) {
               p_forward->number = NULL;
        }

        err = at_tok_nextint(&line, &p_forward->toa);
        if (err < 0) goto error;
    }

return 0;

error:
    LOGE("invalid CCFC line\n");
    return -1;
}

static void requestCallForward(RIL_CallForwardInfo *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    ATLine *p_cur;
    int err;
    char *cmd;

    if (datalen != sizeof(*data))
        goto error;

    if (data->status == 2)
        asprintf(&cmd, "AT+CCFC=%d,%d",
        data->reason,
        data->status);
    else
        asprintf(&cmd, "AT+CCFC=%d,%d,\"%s\",%d,%d",
                 data->reason,
                 data->status,
                 data->number ? data->number : "",
                 data->toa,
                 data->serviceClass);

    err = at_send_command_multiline (ATch_primary, cmd, "+CCFC:", &p_response);
    free(cmd);

    if (err < 0)
        goto error;

    switch (at_get_cme_error(p_response)) {
    case CME_SUCCESS:
    case CME_ERROR_NON_CME:
    break;

    case CME_NOT_ALLOW:
        RIL_onRequestComplete(t, RIL_E_REQUEST_NOT_SUPPORTED, NULL, 0);
        goto done;

    default:
        goto error;
    }

    if (data->status == 2 ) {
        RIL_CallForwardInfo **forwardList, *forwardPool;
        int forwardCount = 0;
        int validCount = 0;
        int i;

        for (p_cur = p_response->p_intermediates
             ; p_cur != NULL
             ; p_cur = p_cur->p_next, forwardCount++
            );

        forwardList = (RIL_CallForwardInfo **)
                      alloca(forwardCount * sizeof(RIL_CallForwardInfo *));

        forwardPool = (RIL_CallForwardInfo *)
                      alloca(forwardCount * sizeof(RIL_CallForwardInfo));

        memset(forwardPool, 0, forwardCount * sizeof(RIL_CallForwardInfo));

        /* init the pointer array */
        for(i = 0; i < forwardCount ; i++)
            forwardList[i] = &(forwardPool[i]);

        for (p_cur = p_response->p_intermediates
             ; p_cur != NULL
             ; p_cur = p_cur->p_next
            ) {
                err = forwardFromCCFCLine(p_cur->line, forwardList[validCount]);

                if (err == 0)
                    validCount++;
        }

        RIL_onRequestComplete(t, RIL_E_SUCCESS,
                              validCount ? forwardList : NULL,
                              validCount * sizeof (RIL_CallForwardInfo *));
    } else
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);

done:
    at_response_free(p_response);
return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestFacilityLock(char **data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int err;
    int result;
    char *cmd, *line;

    if (datalen != 4 * sizeof(char *))
        goto error;

    asprintf(&cmd, "AT+CLCK=\"%s\",%c,\"%s\",%s",
             data[0], *data[1], data[2], data[3]);

    if (*data[1] == '2')
        err = at_send_command_singleline(ATch_primary, cmd, "+CLCK: ",
                                         &p_response);
    else {
        err = at_send_command(ATch_primary, cmd, &p_response);
        if (err < 0 || p_response->success == 0) {
            free(cmd);
            goto error;
        }
        if (*data[1] == '1')
            result = 1;
        else
            result = 0;
        free(cmd);
        RIL_onRequestComplete(t, RIL_E_SUCCESS, &result, sizeof(result));
        at_response_free(p_response);
        return;
    }
    free(cmd);

    if (err < 0 || p_response->success == 0)
        goto error;

    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &result);
    if (err < 0) goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, &result, sizeof(result));
    at_response_free(p_response);
    return;

error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

/** do post-AT+CFUN=1 initialization */
static void onRadioPowerOn()
{
#ifdef USE_TI_COMMANDS
    /*  Must be after CFUN=1 */
    /*  TI specific -- notifications for CPHS things such */
    /*  as CPHS message waiting indicator */

    at_send_command(ATch_primary, "AT%CPHS=1", NULL);

    /*  TI specific -- enable NITZ unsol notifs */
    at_send_command(ATch_primary, "AT%CTZV=1", NULL);

    /* send unsolicited commands at any time */
    at_send_command(ATch_primary, "AT%CUNS=0", NULL);
#endif

    pollSIMState(NULL);
}

static void setSMSstatus(void *param)
{
    ATResponse *p_response = NULL;
    int max_retry = 2;

    simReady = 0;

retry:
    at_send_command_singleline(ATch_primary, "AT+CSMS=1", "+CSMS:", &p_response);

    switch (at_get_cms_error(p_response)) {
        case CMS_BUSY:
		usleep(500000);
                if (0 >= max_retry--) {
                    LOGD("max_retry failed to send AT+CSMS=1 command");
                    goto out;
                }
                at_response_free(p_response);
		goto retry;
                break;
        default:
                break;
    }

    max_retry = 2;
    at_response_free(p_response);
    p_response = NULL;

    /*
     * Always send SMS messages directly to the TE
     *
     * mode = 1 // discard when link is reserved (link should never be
     *             reserved)
     * mt = 2   // most messages routed to TE
     * bm = 2   // new cell BM's routed to TE
     * ds = 1   // Status reports routed to TE
     * bfr = 1  // flush buffer
     */
retry_cnmi:
    at_send_command(ATch_primary, "AT+CNMI=1,2,2,1,1", &p_response);
    switch (at_get_cms_error(p_response)) {
        case CMS_BUSY:
		usleep(500000);
                if (0 >= max_retry--) {
                    LOGD("max_retry failed to send AT+CNMI command");
                    goto out;
                }
                at_response_free(p_response);
		goto retry_cnmi;
                break;
        default:
                break;
    }
    simReady = CSTAT_SMS | CSTAT_PHB;
out:
    at_response_free(p_response);
    p_response = NULL;
    setRadioState(RADIO_STATE_SIM_READY);
}

/** do post- SIM ready initialization */
static void onSIMReady()
{
}

static void requestRadioPower(void *data, size_t datalen, RIL_Token t)
{
    int onOff;

    int err;
    ATResponse *p_response = NULL;

    assert (datalen >= sizeof(int *));
    onOff = ((int *)data)[0];

    if (onOff == 0 && sState != RADIO_STATE_OFF) {
        /* The system ask to shutdown the radio */
        deactivatePDPConnection();
        err = at_send_command(ATch_primary, "AT+CFUN=0", &p_response);
        if (err < 0 || p_response->success == 0) goto error;
        /* reset simReady status */
        simReady = 0;
        setRadioState(RADIO_STATE_OFF);
    } else if (onOff > 0 && sState == RADIO_STATE_OFF) {
        err = at_send_command(ATch_primary, "AT+CFUN=1", &p_response);
        if (err < 0|| p_response->success == 0) {
            // Some stacks return an error when there is no SIM,
            // but they really turn the RF portion on
            // So, if we get an error, let's check to see if it
            // turned on anyway

            if (isRadioOn() != 1) {
                goto error;
            }
        }

#ifdef HAVE_TI_CALYPSO
	/* Increase the incall volume */
        at_send_command(ATch_primary, "AT+CLVL=255", NULL);
#endif
        setRadioState(RADIO_STATE_SIM_NOT_READY);
    }

    at_response_free(p_response);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    return;
error:
    at_response_free(p_response);
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}

static void requestOrSendPDPContextList(RIL_Token *t);

static void onPDPContextListChanged(void *param)
{
    requestOrSendPDPContextList(NULL);
}

static void requestPDPContextList(void *data, size_t datalen, RIL_Token t)
{
    requestOrSendPDPContextList(&t);
}

static void requestOrSendPDPContextList(RIL_Token *t)
{
    ATResponse *p_response;
    ATLine *p_cur;
    int err;
    int n = 0;
    char *out;

    err = at_send_command_multiline (ATch_primary, "AT+CGACT?", "+CGACT:",
                                     &p_response);
    if (err != 0 || p_response->success == 0)
        goto error;

    for (p_cur = p_response->p_intermediates; p_cur != NULL;
         p_cur = p_cur->p_next)
        n++;

    RIL_Data_Call_Response *responses =
        alloca(n * sizeof(RIL_Data_Call_Response));

    int i;
    for (i = 0; i < n; i++) {
        responses[i].cid = -1;
        responses[i].active = -1;
        responses[i].type = "";
        responses[i].apn = "";
        responses[i].address = "";
    }

    RIL_Data_Call_Response *response = responses;
    for (p_cur = p_response->p_intermediates; p_cur != NULL;
         p_cur = p_cur->p_next) {
        char *line = p_cur->line;

        err = at_tok_start(&line);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &response->cid);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &response->active);
        if (err < 0)
            goto error;

        response++;
    }

    at_response_free(p_response);

    err = at_send_command_multiline (ATch_primary, "AT+CGDCONT?", "+CGDCONT:",
                                     &p_response);
    if (err != 0 || p_response->success == 0)
        goto error;

    for (p_cur = p_response->p_intermediates; p_cur != NULL;
         p_cur = p_cur->p_next) {
        char *line = p_cur->line;
        int cid;
        char *type;
        char *apn;
        char *address;

        err = at_tok_start(&line);
        if (err < 0)
            goto error;

        err = at_tok_nextint(&line, &cid);
        if (err < 0)
            goto error;

        for (i = 0; i < n; i++) {
            if (responses[i].cid == cid)
                break;
        }

        if (i >= n) {
            /* details for a context we didn't hear about in the last request */
            continue;
        }

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].type = alloca(strlen(out) + 1);
        strcpy(responses[i].type, out);

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].apn = alloca(strlen(out) + 1);
        strcpy(responses[i].apn, out);

        err = at_tok_nextstr(&line, &out);
        if (err < 0)
            goto error;

        responses[i].address = alloca(strlen(out) + 1);
        strcpy(responses[i].address, out);
    }

    at_response_free(p_response);

    if (t != NULL)
        RIL_onRequestComplete(*t, RIL_E_SUCCESS, responses,
                              n * sizeof(RIL_Data_Call_Response));
    else
        RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                  responses,
                                  n * sizeof(RIL_Data_Call_Response));

    return;

error:
    if (t != NULL)
        RIL_onRequestComplete(*t, RIL_E_GENERIC_FAILURE, NULL, 0);
    else
        RIL_onUnsolicitedResponse(RIL_UNSOL_DATA_CALL_LIST_CHANGED,
                                  NULL, 0);
    at_response_free(p_response);
}

static void requestNetworkList(void *data, size_t datalen, RIL_Token t)
{
    static char *statStr[] = {
        "unknown",
        "available",
        "current",
        "forbidden"
    };
    int err, count, stat;
    char *line, *tok;
    char **response, **cur;
    ATResponse *p_response = NULL;

    err = at_send_command_singleline(ATch_primary, "AT+COPS=?", "+COPS:",
                                     &p_response);

    if (err != 0 || p_response->success == 0) goto error;

    line = p_response->p_intermediates->line;

    for (count = 0; *line; line++)
        if (*line == ')')
            count++;

    response = alloca(count * 4 * sizeof(char *));
    if (!response) goto error;

    line = p_response->p_intermediates->line;
    count = 0;
    cur = response;

    while ((line = strchr(line, '('))) {
        line++;

        err = at_tok_nextint(&line, &stat);
        if (err < 0) continue;

        cur[3] = statStr[stat];

        err = at_tok_nextstr(&line, &(cur[0]));
        if (err < 0) continue;

        err = at_tok_nextstr(&line, &(cur[1]));
        if (err < 0) continue;

        err = at_tok_nextstr(&line, &(cur[2]));
        if (err < 0) continue;
            cur += 4;
        count++;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, response, count * 4 * sizeof(char *));
    at_response_free(p_response);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestQueryNetworkSelectionMode(
                void *data, size_t datalen, RIL_Token t)
{
    int err;
    ATResponse *p_response = NULL;
    int response = 0;
    char *line;

    err = at_send_command_singleline(ATch_primary, "AT+COPS?", "+COPS:", &p_response);

    if (err < 0 || p_response->success == 0)
        goto error;

    /* take a look at the manual AT to divedes the fields */
    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);

    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &response);
    if (err < 0) goto error;

    at_response_free(p_response);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, &response, sizeof(response));
    return;
error:
    at_response_free(p_response);
    LOGE("requestQueryNetworkSelectionMode must never return error when radio is on");
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}

static void requestNetworkRegistration(void *data, size_t datalen, RIL_Token t)
{
    char *network;
    char *cmd;
    int err;
    network = (char *)data;
    if (network)
        asprintf(&cmd, "AT+COPS=1,2,\"%s\"", network);
    else
        goto error;
    at_send_command(ATch_primary, cmd, NULL);
    free(cmd);
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
}

static void receiveSMS(void *param)
{
    int location = (int)param;
    char *cmd;

    if (screenStatus) {
        asprintf(&cmd, "AT+CMGR=%d", location);
        /* request the sms in a specific location */
        at_send_command(ATch_primary, cmd, NULL);
        free(cmd);
        /* remove the sms from specific location XXX temp fix*/
        asprintf(&cmd, "AT+CMGD=%d,0", location);
        at_send_command(ATch_primary, cmd, NULL);
        free(cmd);
    } else
            RIL_requestTimedCallback (receiveSMS, param, &TIMEVAL_SIMPOLL);
    return;
}

static void sendCallStateChanged(void *param)
{
    RIL_onUnsolicitedResponse (
        RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
        NULL, 0);
}

static void requestGetCurrentCalls(void *data, size_t datalen, RIL_Token t)
{
    int err;
    ATResponse *p_response;
    ATLine *p_cur;
    int countCalls;
    int countValidCalls;
    RIL_Call *p_calls;
    RIL_Call **pp_calls;
    int i;
    int needRepoll = 0;

#ifdef WORKAROUND_ERRONEOUS_ANSWER
    int prevIncomingOrWaitingLine;

    prevIncomingOrWaitingLine = s_incomingOrWaitingLine;
    s_incomingOrWaitingLine = -1;
#endif /*WORKAROUND_ERRONEOUS_ANSWER*/

    err = at_send_command_multiline (ATch_primary, "AT+CLCC", "+CLCC:",
                                     &p_response);

    if (err != 0 || p_response->success == 0) {
        RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
        at_response_free(p_response);
        return;
    }

    /* count the calls */
    for (countCalls = 0, p_cur = p_response->p_intermediates
            ; p_cur != NULL
            ; p_cur = p_cur->p_next
    ) {
        countCalls++;
    }

    /* yes, there's an array of pointers and then an array of structures */

    pp_calls = (RIL_Call **)alloca(countCalls * sizeof(RIL_Call *));
    p_calls = (RIL_Call *)alloca(countCalls * sizeof(RIL_Call));
    memset (p_calls, 0, countCalls * sizeof(RIL_Call));

    /* init the pointer array */
    for(i = 0; i < countCalls ; i++) {
        pp_calls[i] = &(p_calls[i]);
    }

    for (countValidCalls = 0, p_cur = p_response->p_intermediates
            ; p_cur != NULL
            ; p_cur = p_cur->p_next
    ) {
        err = callFromCLCCLine(p_cur->line, p_calls + countValidCalls);

        if (err != 0) {
            continue;
        }

#ifdef WORKAROUND_ERRONEOUS_ANSWER
        if (p_calls[countValidCalls].state == RIL_CALL_INCOMING
            || p_calls[countValidCalls].state == RIL_CALL_WAITING
        ) {
            s_incomingOrWaitingLine = p_calls[countValidCalls].index;
        }
#endif /*WORKAROUND_ERRONEOUS_ANSWER*/

        if (p_calls[countValidCalls].state != RIL_CALL_ACTIVE
            && p_calls[countValidCalls].state != RIL_CALL_HOLDING
        ) {
            needRepoll = 1;
        }

        countValidCalls++;
    }

#ifdef WORKAROUND_ERRONEOUS_ANSWER
    // Basically:
    // A call was incoming or waiting
    // Now it's marked as active
    // But we never answered it
    //
    // This is probably a bug, and the call will probably
    // disappear from the call list in the next poll
    if (prevIncomingOrWaitingLine >= 0
            && s_incomingOrWaitingLine < 0
            && s_expectAnswer == 0
    ) {
        for (i = 0; i < countValidCalls ; i++) {

            if (p_calls[i].index == prevIncomingOrWaitingLine
                    && p_calls[i].state == RIL_CALL_ACTIVE
                    && s_repollCallsCount < REPOLL_CALLS_COUNT_MAX
            ) {
                LOGI(
                    "Hit WORKAROUND_ERRONOUS_ANSWER case."
                    " Repoll count: %d\n", s_repollCallsCount);
                s_repollCallsCount++;
                goto error;
            }
        }
    }

    s_expectAnswer = 0;
    s_repollCallsCount = 0;
#endif /*WORKAROUND_ERRONEOUS_ANSWER*/

    RIL_onRequestComplete(t, RIL_E_SUCCESS, pp_calls,
            countValidCalls * sizeof (RIL_Call *));

    at_response_free(p_response);

#ifdef POLL_CALL_STATE
    if (countValidCalls) {  // We don't seem to get a "NO CARRIER" message from
                            // smd, so we're forced to poll until the call ends.
#else
    if (needRepoll) {
#endif
        RIL_requestTimedCallback (sendCallStateChanged, NULL, &TIMEVAL_CALLSTATEPOLL);
    }

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestDial(void *data, size_t datalen, RIL_Token t)
{
    RIL_Dial *p_dial;
    char *cmd;
    const char *clir;
    int ret;

    p_dial = (RIL_Dial *)data;

    switch (p_dial->clir) {
        case 1: clir = "I"; break;  /*invocation*/
        case 2: clir = "i"; break;  /*suppression*/
        default:
        case 0: clir = ""; break;   /*subscription default*/
    }

#ifdef HAVE_TI_CALYPSO
    ret = at_send_command(ATch_primary, "AT@ST=\"-26\"", NULL);
    ret = at_send_command(ATch_primary, "AT%N0187", NULL);
#endif
    asprintf(&cmd, "ATD%s%s;", p_dial->address, clir);

    ret = at_send_command(ATch_primary, cmd, NULL);
    free(cmd);

    /* success or failure is ignored by the upper layer here.
       it will call GET_CURRENT_CALLS and determine success that way */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

static void requestWriteSmsToSim(void *data, size_t datalen, RIL_Token t)
{
    RIL_SMS_WriteArgs *p_args;
    char *cmd;
    int length;
    int err;
    ATResponse *p_response = NULL;

    p_args = (RIL_SMS_WriteArgs *)data;

    length = strlen(p_args->pdu)/2;
    asprintf(&cmd, "AT+CMGW=%d,%d", length, p_args->status);

    err = at_send_command_sms(ATch_primary, cmd, p_args->pdu, "+CMGW:",
                              &p_response);
    free(cmd);

    if (err != 0 || p_response->success == 0) goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    at_response_free(p_response);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestHangup(void *data, size_t datalen, RIL_Token t)
{
    int *p_line;
    ATResponse *p_response = NULL;

    int ret;
    char *cmd;

    p_line = (int *)data;

    // 3GPP 22.030 6.5.5
    // "Releases a specific active call X"
    asprintf(&cmd, "AT+CHLD=1%d", p_line[0]);
    ret = at_send_command(ATch_primary, cmd, &p_response);

    switch (at_get_cme_error(p_response)) {
        case CME_UNKNOWN_ERROR:
        case CME_NOT_ALLOW:
            /* the call is not active, hang the active call */
            ret = at_send_command(ATch_primary, "ATH", NULL);
            break;
        default:
            break;
    }

    at_response_free(p_response);
    free(cmd);

    /* success or failure is ignored by the upper layer here.
       it will call GET_CURRENT_CALLS and determine success that way */
    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
}

static void requestSignalStrength(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int err;
    int response[2];
    char *line;

    err = at_send_command_singleline(ATch_primary, "AT+CSQ", "+CSQ:",
                                     &p_response);

    if (err < 0 || p_response->success == 0)
        goto error;

    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(response[0]));
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(response[1]));
    if (err < 0) goto error;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));

    at_response_free(p_response);
    return;

error:
    LOGE("requestSignalStrength must never return an error when radio is on");
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestRegistrationState(int request, void *data,
                                        size_t datalen, RIL_Token t)
{
    int err;
    int response[4];
    char * responseStr[4];
    ATResponse *p_response = NULL;
    const char *cmd;
    const char *prefix;
    char *line, *p;
    int commas;
    int skip;
    int count = 3;


    if (request == RIL_REQUEST_REGISTRATION_STATE) {
        cmd = "AT+CREG?";
        prefix = "+CREG:";
    } else if (request == RIL_REQUEST_GPRS_REGISTRATION_STATE) {
        cmd = "AT+CGREG?";
        prefix = "+CGREG:";
    } else {
        assert(0);
        goto error;
    }

    err = at_send_command_singleline(ATch_primary, cmd, prefix, &p_response);
    if (err != 0) goto error;

    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    /* Ok you have to be careful here
     * The solicited version of the CREG response is
     * +CREG: n, stat, [lac, cid]
     * and the unsolicited version is
     * +CREG: stat, [lac, cid]
     * The <n> parameter is basically "is unsolicited creg on?"
     * which it should always be
     *
     * Now we should normally get the solicited version here,
     * but the unsolicited version could have snuck in
     * so we have to handle both
     *
     * Also since the LAC and CID are only reported when registered,
     * we can have 1, 2, 3, or 4 arguments here
     *
     * finally, a +CGREG: answer may have a fifth value that corresponds
     * to the network type, as in;
     *
     *   +CGREG: n, stat [,lac, cid [,networkType]]
     */

    /* count number of commas */
    commas = 0;
    for (p = line ; *p != '\0' ;p++) {
        if (*p == ',') commas++;
    }

    switch (commas) {
        case 0: /* +CREG: <stat> */
            err = at_tok_nextint(&line, &response[0]);
            if (err < 0) goto error;
            response[1] = -1;
            response[2] = -1;
        break;

        case 1: /* +CREG: <n>, <stat> */
            err = at_tok_nextint(&line, &skip);
            if (err < 0) goto error;
            err = at_tok_nextint(&line, &response[0]);
            if (err < 0) goto error;
            response[1] = -1;
            response[2] = -1;
            if (err < 0) goto error;
        break;

        case 2: /* +CREG: <stat>, <lac>, <cid> */
            err = at_tok_nextint(&line, &response[0]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[1]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[2]);
            if (err < 0) goto error;
        break;
        case 3: /* +CREG: <n>, <stat>, <lac>, <cid> */
            err = at_tok_nextint(&line, &skip);
            if (err < 0) goto error;
            err = at_tok_nextint(&line, &response[0]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[1]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[2]);
            if (err < 0) goto error;
        break;
        /* special case for CGREG, there is a fourth parameter
         * that is the network type (unknown/gprs/edge/umts)
         */
        case 4: /* +CGREG: <n>, <stat>, <lac>, <cid>, <networkType> */
            err = at_tok_nextint(&line, &skip);
            if (err < 0) goto error;
            err = at_tok_nextint(&line, &response[0]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[1]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[2]);
            if (err < 0) goto error;
            err = at_tok_nexthexint(&line, &response[3]);
            if (err < 0) goto error;
            count = 4;
        break;
        default:
            goto error;
    }

#ifdef FAKE_HAVE_TI_CALYPSO
    if (request == RIL_REQUEST_REGISTRATION_STATE) {
        /* This save the internal network status */
        internalNetworkStatusRegistration(response[0], response[2]);
    }
#endif

    asprintf(&responseStr[0], "%d", response[0]);
    asprintf(&responseStr[1], "%d", response[1]);
    asprintf(&responseStr[2], "%d", response[2]);

    if (count > 3)
        asprintf(&responseStr[3], "%d", response[3]);

    RIL_onRequestComplete(t, RIL_E_SUCCESS, responseStr, count*sizeof(char*));
    at_response_free(p_response);
    free(responseStr[0]);
    free(responseStr[1]);
    free(responseStr[2]);
    if (count > 3)
        free(responseStr[3]);

    return;
error:
    LOGE("requestRegistrationState must never return an error when"
         " radio is on");
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestNeighboaringCellIds(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    RIL_NeighboringCell *NeighboringCell;
    RIL_NeighboringCell **NeighboringCellList;
    int err = 0;
    int cell_id_number;
    int current = 0;
    char *line;
    ATLine *p_cur;

    err = at_send_command_multiline(ATch_primary, "AT%EM=2,3", "%EM:",
                                    &p_response);
    if (err != 0 || p_response->success == 0)
        goto error;

    p_cur = p_response->p_intermediates;
    line = p_cur->line;

    err = at_tok_start(&line);
    if (err < 0)
        goto error;

    err = at_tok_nextint(&line, &cell_id_number);
    if (err < 0 || cell_id_number == 0)
        goto error;

    NeighboringCellList = (RIL_NeighboringCell **) alloca(cell_id_number *
                                 sizeof(RIL_NeighboringCell *));

    NeighboringCell = (RIL_NeighboringCell *)
                      alloca(cell_id_number * sizeof(RIL_NeighboringCell));

    p_cur = p_cur->p_next;

    /* Message format
     * arfcn	BCCH channel	See frequency calculation note
     * c1	Path Loss Criterion C1
     * c2	Cell-reselection Criterion C2
     * rxlev 	Received Field Strength (rxlev/2)+2 gives the AT+CSQ
     *		response value
     * bsic	Base Station ID Code
     * cell_id	Cell Indentity
     */

    for (current = 0; p_cur != NULL, current < cell_id_number;
         p_cur = p_cur->p_next,
         current++) {

        int skip, i;
        line = p_cur->line;

        for (i = 0; i < 3;  i++) {
            err = at_tok_nextint(&line, &skip);
            if (err < 0)
                goto error;
        }

        err = at_tok_nextint(&line, &(NeighboringCell[current].rssi));
        if (err < 0)
            goto error;

        NeighboringCell[current].rssi = (NeighboringCell[current].rssi / 2) + 2;

        err = at_tok_nextint(&line, &skip);
        if (err < 0)
            goto error;

        err = at_tok_nextstr(&line, &(NeighboringCell[current].cid));
        if (err < 0)
            goto error;

        LOGD("Add cell_id %s = %d", NeighboringCell[current].cid,
             NeighboringCell[current].rssi);

        NeighboringCellList[current] = &NeighboringCell[current];
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NeighboringCellList,
                          cell_id_number * sizeof(RIL_NeighboringCell *));
    at_response_free(p_response);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestOperator(void *data, size_t datalen, RIL_Token t)
{
    int err;
    int i;
    int skip;
    ATLine *p_cur;
    char *response[3];

    memset(response, 0, sizeof(response));

    ATResponse *p_response = NULL;

    err = at_send_command_multiline(ATch_primary,
        "AT+COPS=3,0;+COPS?;+COPS=3,1;+COPS?;+COPS=3,2;+COPS?",
        "+COPS:", &p_response);

    /* we expect 3 lines here:
     * +COPS: 0,0,"T - Mobile"
     * +COPS: 0,1,"TMO"
     * +COPS: 0,2,"310170"
     */

    if (err != 0) goto error;

    for (i = 0, p_cur = p_response->p_intermediates
            ; p_cur != NULL
            ; p_cur = p_cur->p_next, i++
    ) {
        char *line = p_cur->line;

        err = at_tok_start(&line);
        if (err < 0) goto error;

        err = at_tok_nextint(&line, &skip);
        if (err < 0) goto error;

        // If we're unregistered, we may just get
        // a "+COPS: 0" response
        if (!at_tok_hasmore(&line)) {
            response[i] = NULL;
            continue;
        }

        err = at_tok_nextint(&line, &skip);
        if (err < 0) goto error;

        // a "+COPS: 0, n" response is also possible
        if (!at_tok_hasmore(&line)) {
            response[i] = NULL;
            continue;
        }

        err = at_tok_nextstr(&line, &(response[i]));
        if (err < 0) goto error;
    }

    if (i != 3) {
        /* expect 3 lines exactly */
        goto error;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));
    at_response_free(p_response);

    return;
error:
    LOGE("requestOperator must not return error when radio is on");
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestSendSMS(void *data, size_t datalen, RIL_Token t)
{
    int err;
    const char *smsc;
    const char *pdu;
    int tpLayerLength;
    char *cmd1, *cmd2;
    RIL_SMS_Response response;
    ATResponse *p_response = NULL;

    smsc = ((const char **)data)[0];
    pdu = ((const char **)data)[1];

    tpLayerLength = strlen(pdu)/2;

    // "NULL for default SMSC"
    if (smsc == NULL) {
        smsc= "00";
    }

    asprintf(&cmd1, "AT+CMGS=%d", tpLayerLength);
    asprintf(&cmd2, "%s%s", smsc, pdu);

    err = at_send_command_sms(ATch_primary, cmd1, cmd2, "+CMGS:", &p_response);
    free(cmd1);
    free(cmd2);
    if (err != 0 || p_response->success == 0) goto error;

    memset(&response, 0, sizeof(response));

    /* FIXME fill in messageRef and ackPDU */

    RIL_onRequestComplete(t, RIL_E_SUCCESS, &response, sizeof(response));
    at_response_free(p_response);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}

static void requestSetupDefaultPDP(void *data, size_t datalen, RIL_Token t)
{
    const char *apn;
    char *cmd;
    int err;
    ATResponse *p_response = NULL;
    char *response[2] = { "1", "gprs" };

    apn = ((const char **)data)[0];

#ifdef FAKE_USE_TI_COMMANDS
    // Config for multislot class 10 (probably default anyway eh?)
    err = at_send_command(ATch_primary, "AT%CPRIM=\"GMM\",\"CONFIG MULTISLOT_CLASS=<10>\"",
                        NULL);

    err = at_send_command(ATch_primary, "AT%DATA=2,\"UART\",1,,\"SER\",\"UART\",0", NULL);
#endif /* USE_TI_COMMANDS */

    int fd, qmistatus;
    size_t cur = 0;
    size_t len;
    ssize_t written, rlen;
    char status[32] = {0};
    int retry = 10;

    LOGD("requesting data connection to APN '%s'", apn);

    fd = open ("/dev/qmi", O_RDWR);
    if (fd >= 0) { /* the device doesn't exist on the emulator */

        LOGD("opened the qmi device\n");
        asprintf(&cmd, "up:%s", apn);
        len = strlen(cmd);

        while (cur < len) {
            do {
                written = write (fd, cmd + cur, len - cur);
            } while (written < 0 && errno == EINTR);

            if (written < 0) {
                LOGE("### ERROR writing to /dev/qmi");
                close(fd);
                goto error;
            }

            cur += written;
        }

        // wait for interface to come online

        do {
            sleep(1);
            do {
                rlen = read(fd, status, 31);
            } while (rlen < 0 && errno == EINTR);

            if (rlen < 0) {
                LOGE("### ERROR reading from /dev/qmi");
                close(fd);
                goto error;
            } else {
                status[rlen] = '\0';
                LOGD("### status: %s", status);
            }
        } while (strncmp(status, "STATE=up", 8) && strcmp(status, "online") && --retry);

        close(fd);

        if (retry == 0) {
            LOGE("### Failed to get data connection up\n");
	        goto error;
        }

        qmistatus = system("netcfg rmnet0 dhcp");

        LOGD("netcfg rmnet0 dhcp: status %d\n", qmistatus);

        if (qmistatus < 0) goto error;

    } else if (ATch_gprs) {
        const char *username = ((const char **)data)[1];
        const char *password = ((const char **)data)[2];

        writePppOptions(0, gprs_device, username, password);

        #ifdef HAVE_TI_CALYPSO
        write(ATch_gprs->s_fd, "\x1a", 1);
        usleep(250000);
        #endif

        deactivatePDPConnection();

        ATch_gprs->nolog = 0;

        asprintf(&cmd, "AT+CGDCONT=1,\"IP\",\"%s\",,0,0", apn);
        //FIXME check for error here
        err = at_send_command(ATch_gprs, cmd, NULL);
        free(cmd);

        // Set required QoS params to default
        err = at_send_command(ATch_gprs, "AT+CGQREQ=1", NULL);

        // Set minimum QoS params to default
        err = at_send_command(ATch_gprs, "AT+CGQMIN=1", NULL);

        // packet-domain event reporting
        err = at_send_command(ATch_gprs, "AT+CGEREP=1,0", NULL);

        // Hangup anything that's happening there now
        err = at_send_command(ATch_gprs, "AT+CGACT=1,0", NULL);

        // Start data on PDP context 1
        err = at_send_command(ATch_gprs, "ATD*99***1#", &p_response);
        ATch_gprs->nolog = 1;

        if (err < 0 || p_response->success == 0) {
            goto error;
        }
    } else
        goto error;

    gprs_status = 1;
    RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));
    at_response_free(p_response);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);

}

static void getIMEInumber(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    int err;
    char *line;
    char *imei;

    err = at_send_command_singleline(ATch_primary, "AT+CGSN", "+CGSN:",
                                     &p_response);
    if (err < 0)
        goto error;

    line = p_response->p_intermediates->line;

    err = at_tok_start (&line);
    if (err < 0) {
        LOGD("IMEI %s line", line);
        goto error;
    }

    err = at_tok_nextstr(&line, &imei);
    if (err < 0) {
        LOGD("IMEI %s imei", imei);
        goto error;
    }

    LOGD("IMEI %s imei", imei);
    RIL_onRequestComplete(t, RIL_E_SUCCESS,
                          imei, sizeof(imei));
    at_response_free(p_response);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
    return;
}

static void requestSMSAcknowledge(void *data, size_t datalen, RIL_Token t)
{
    int ackSuccess;
    int err;

    ackSuccess = ((int *)data)[0];

    if (ackSuccess == 1) {
        err = at_send_command(ATch_primary, "AT+CNMA=1", NULL);
    } else if (ackSuccess == 0)  {
        err = at_send_command(ATch_primary, "AT+CNMA=2", NULL);
    } else {
        LOGE("unsupported arg to RIL_REQUEST_SMS_ACKNOWLEDGE\n");
        goto error;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);

}

static void  requestSIM_IO(void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response = NULL;
    RIL_SIM_IO_Response sr;
    int err;
    char *cmd = NULL;
    RIL_SIM_IO *p_args;
    char *line;

    memset(&sr, 0, sizeof(sr));

    p_args = (RIL_SIM_IO *)data;

    /* FIXME handle pin2 */
    if (p_args->data == NULL) {
#ifdef HAVE_TI_CALYPSO
        /* Select command. */
        if ((p_args->fileid == 28474 || p_args->fileid == 28476)
            && p_args->command == 192) {
            char *tmp;
            int len;
            at_send_command_singleline(ATch_primary,
                                       "AT+CSIM=14,\"A0A40000027F10\"",
                                       "+CSIM:", NULL);
            asprintf(&cmd, "AT+CSIM=14,\"A0A4000002%04x\"", p_args->fileid);
            at_send_command_singleline(ATch_primary, cmd, "+CSIM:", NULL);
            free(cmd);
            asprintf(&cmd, "AT+CSIM=10,\"A0%02x%02x%02x%02x\"", p_args->command,
                     p_args->p1, p_args->p2, p_args->p3);

            err = at_send_command_singleline(ATch_primary, cmd,
                                             "+CSIM:", &p_response);
            free(cmd);
            if (err < 0 || p_response->success == 0) {
                goto parse_error;
            }

            line = p_response->p_intermediates->line;

            err = at_tok_start(&line);
            if (err < 0) goto parse_error;

            err = at_tok_nextint(&line, &len);
            if (err < 0) goto parse_error;

            tmp =  strndup(line, len);

            sr.simResponse = tmp;
            sr.sw1 = 0x90;
            sr.sw2 = 0;

            RIL_onRequestComplete(t, RIL_E_SUCCESS, &sr, sizeof(sr));
            at_response_free(p_response);
            free(tmp);
            return;
parse_error:
            RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
            at_response_free(p_response);
            return;
         } else
#endif
        asprintf(&cmd, "AT+CRSM=%d,%d,%d,%d,%d",
                    p_args->command, p_args->fileid,
                    p_args->p1, p_args->p2, p_args->p3);
    } else {
        asprintf(&cmd, "AT+CRSM=%d,%d,%d,%d,%d,%s",
                    p_args->command, p_args->fileid,
                    p_args->p1, p_args->p2, p_args->p3, p_args->data);
    }

    err = at_send_command_singleline(ATch_primary, cmd, "+CRSM:", &p_response);

    if (err < 0 || p_response->success == 0) {
        goto error;
    }

    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(sr.sw1));
    if (err < 0) goto error;

    err = at_tok_nextint(&line, &(sr.sw2));
    if (err < 0) goto error;

    if (at_tok_hasmore(&line)) {
        err = at_tok_nextstr(&line, &(sr.simResponse));
        if (err < 0) goto error;
    }

    RIL_onRequestComplete(t, RIL_E_SUCCESS, &sr, sizeof(sr));
    at_response_free(p_response);
    free(cmd);

    return;
error:
    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
    free(cmd);

}

static void  requestScreeState(int status, RIL_Token t)
{
#ifdef HAVE_TI_CALYPSO
    if (!status) {
        /* Suspend */
        at_send_command(ATch_primary, "AT%CPRI=0", NULL);
        at_send_command(ATch_primary, "AT%CSQ=0", NULL);
        at_send_command(ATch_primary, "AT+CREG=0", NULL);
        at_send_command(ATch_primary, "AT+CGREG=0", NULL);
        at_send_command(ATch_primary, "AT+CGEREP=0,0", NULL);
    } else {
        /* Resume */
        at_send_command(ATch_primary, "AT+CLVL=255", NULL);
        at_send_command(ATch_primary, "AT%CNIV=1", NULL);
        at_send_command(ATch_primary, "AT%CPRI=1", NULL);
        at_send_command(ATch_primary, "AT%CSQ=1", NULL);
        at_send_command(ATch_primary, "AT+CREG=2", NULL);
        at_send_command(ATch_primary, "AT+CGREG=2", NULL);
        at_send_command(ATch_primary, "AT+CGEREP=2,1", NULL);
        if (sState == RADIO_STATE_SIM_READY)
            onSIMReady();
    }

    /* we must check the screen status before read and sms in the
     * memory */
    screenStatus = status;

    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
#else //HAVE_TI_CALYPSO
    RIL_onRequestComplete(t, RIL_E_REQUEST_NOT_SUPPORTED, NULL, 0);
#endif
}

static void  requestEnterSimPin(void*  data, size_t  datalen, RIL_Token  t)
{
    ATResponse   *p_response = NULL;
    int           err;
    char*         cmd = NULL;
    const char**  strings = (const char**)data;;

    if ( datalen == sizeof(char*) ) {
        asprintf(&cmd, "AT+CPIN=\"%s\"", strings[0]);
    } else if ( datalen == 2*sizeof(char*) ) {
        asprintf(&cmd, "AT+CPIN=\"%s,%s\"", strings[0], strings[1]);
    } else
        goto error;

    err = at_send_command(ATch_primary, cmd, &p_response);
    free(cmd);

    if (err < 0 || p_response->success == 0) {
error:
        RIL_onRequestComplete(t, RIL_E_PASSWORD_INCORRECT, NULL, 0);
    } else {
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    }
    at_response_free(p_response);
}

static void  requestSendUSSD(void *data, size_t datalen, RIL_Token t)
{
    ATResponse   *p_response = NULL;
    const char *ussdRequest;
    int err;
    char *cmd;

    ussdRequest = (char *)(data);

    /* GSM character set */
    at_send_command(ATch_primary, "AT+CSCS=\"GSM\"", NULL);

    asprintf(&cmd, "AT+CUSD=1,\"%s\",15", ussdRequest);
    err = at_send_command(ATch_primary, cmd, &p_response);
    free(cmd);

    if (err < 0 || p_response->success == 0) {
        RIL_onRequestComplete(t, RIL_E_PASSWORD_INCORRECT, NULL, 0);
    } else {
        RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
    }
    at_response_free(p_response);
}


/*** Callback methods from the RIL library to us ***/

/**
 * Call from RIL to us to make a RIL_REQUEST
 *
 * Must be completed with a call to RIL_onRequestComplete()
 *
 * RIL_onRequestComplete() may be called from any thread, before or after
 * this function returns.
 *
 * Will always be called from the same thread, so returning here implies
 * that the radio is ready to process another command (whether or not
 * the previous command has completed).
 */
static void
onRequest (int request, void *data, size_t datalen, RIL_Token t)
{
    ATResponse *p_response;
    int err;

    LOGD("onRequest: %s", requestToString(request));

    /* Ignore all requests except RIL_REQUEST_GET_SIM_STATUS
     * when RADIO_STATE_UNAVAILABLE.
     */
    if (sState == RADIO_STATE_UNAVAILABLE
        && request != RIL_REQUEST_GET_SIM_STATUS
    ) {
        RIL_onRequestComplete(t, RIL_E_RADIO_NOT_AVAILABLE, NULL, 0);
        return;
    }

    /* Ignore all non-power requests when RADIO_STATE_OFF
     * (except RIL_REQUEST_GET_SIM_STATUS)
     */
    if (sState == RADIO_STATE_OFF
        && !(request == RIL_REQUEST_RADIO_POWER
            || request == RIL_REQUEST_GET_SIM_STATUS)
    ) {
        RIL_onRequestComplete(t, RIL_E_RADIO_NOT_AVAILABLE, NULL, 0);
        return;
    }

    switch (request) {
        case RIL_REQUEST_QUERY_FACILITY_LOCK:
        {
            char *lockData[4];
            lockData[0] = ((char **)data)[0];
            lockData[1] = FACILITY_LOCK_REQUEST;
            lockData[2] = ((char **)data)[1];
            lockData[3] = ((char **)data)[2];
            requestFacilityLock(lockData, datalen + sizeof(char *), t);
            break;
        }

        case RIL_REQUEST_SET_FACILITY_LOCK:
            requestFacilityLock(data, datalen, t);
            break;

        case RIL_REQUEST_QUERY_CALL_FORWARD_STATUS:
        {
            RIL_CallForwardInfo request;
            request.status = ((int *)data)[0];
            request.reason = ((int *)data)[1];
            requestCallForward(&request, sizeof(request), t);
            break;
        }

        case RIL_REQUEST_SET_CALL_FORWARD:
            requestCallForward(data, datalen, t);
            break;

        case RIL_REQUEST_GET_SIM_STATUS: {
            RIL_CardStatus *p_card_status;
            char *p_buffer;
            int buffer_size;

            int result = getCardStatus(&p_card_status);
            if (result == RIL_E_SUCCESS) {
                p_buffer = (char *)p_card_status;
                buffer_size = sizeof(*p_card_status);
            } else {
                p_buffer = NULL;
                buffer_size = 0;
            }
            RIL_onRequestComplete(t, result, p_buffer, buffer_size);
            freeCardStatus(p_card_status);
            break;
        }

        case RIL_REQUEST_GET_CURRENT_CALLS:
            requestGetCurrentCalls(data, datalen, t);
            break;
        case RIL_REQUEST_DIAL:
            requestDial(data, datalen, t);
            break;
        case RIL_REQUEST_HANGUP:
            requestHangup(data, datalen, t);
            break;
        case RIL_REQUEST_HANGUP_WAITING_OR_BACKGROUND: {
            // 3GPP 22.030 6.5.5
            // "Releases all held calls or sets User Determined User Busy
            //  (UDUB) for a waiting call."
            ATResponse *p_response = NULL;

            at_send_command(ATch_primary, "AT+CHLD=0", &p_response);
            switch (at_get_cme_error(p_response)) {
                case CME_UNKNOWN_ERROR:
                case CME_NOT_ALLOW:
                    at_send_command(ATch_primary, "ATH", NULL);
                    break;
                default:
                    break;
            }
            at_response_free(p_response);
            p_response = NULL;

            /* success or failure is ignored by the upper layer here.
               it will call GET_CURRENT_CALLS and determine success that way */
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
            }
        case RIL_REQUEST_HANGUP_FOREGROUND_RESUME_BACKGROUND: {
                // 3GPP 22.030 6.5.5
                // "Releases all active calls (if any exist) and accepts
                //  the other (held or waiting) call."
                int cms_ret, cme_ret;
                ATResponse *p_response = NULL;
                at_send_command(ATch_primary, "AT+CHLD=1", &p_response);
                cme_ret = at_get_cme_error(p_response);
                cms_ret = at_get_cms_error(p_response);
                if (cms_ret == CMS_SUCCESS && cme_ret == CME_SUCCESS)
                    goto ok_answer;

                if (cms_ret != CME_ERROR_NON_CME ||
                    cme_ret != CMS_ERROR_NON_CMS) {
                        at_send_command(ATch_primary, "ATH", NULL);
		}
ok_answer:
                at_response_free(p_response);
                p_response = NULL;
                /* success or failure is ignored by the upper layer here.
                it will call GET_CURRENT_CALLS and determine success that way */
                RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
                break;
            }
        case RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE:
            // 3GPP 22.030 6.5.5
            // "Places all active calls (if any exist) on hold and accepts
            //  the other (held or waiting) call."
            at_send_command(ATch_primary, "AT+CHLD=2", NULL);

#ifdef WORKAROUND_ERRONEOUS_ANSWER
            s_expectAnswer = 1;
#endif /* WORKAROUND_ERRONEOUS_ANSWER */

            /* success or failure is ignored by the upper layer here.
               it will call GET_CURRENT_CALLS and determine success that way */
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        case RIL_REQUEST_ANSWER:
#ifdef HAVE_TI_CALYPSO
            at_send_command(ATch_primary, "AT@ST=\"-26\"", NULL);
            at_send_command(ATch_primary, "AT%N0187", NULL);
#endif
            at_send_command(ATch_primary, "ATA", NULL);

#ifdef WORKAROUND_ERRONEOUS_ANSWER
            s_expectAnswer = 1;
#endif /* WORKAROUND_ERRONEOUS_ANSWER */

            /* success or failure is ignored by the upper layer here.
               it will call GET_CURRENT_CALLS and determine success that way */
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        case RIL_REQUEST_CONFERENCE:
            // 3GPP 22.030 6.5.5
            // "Adds a held call to the conversation"
            at_send_command(ATch_primary, "AT+CHLD=3", NULL);

            /* success or failure is ignored by the upper layer here.
               it will call GET_CURRENT_CALLS and determine success that way */
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        case RIL_REQUEST_UDUB:
            /* user determined user busy */
            /* sometimes used: ATH */
            at_send_command(ATch_primary, "ATH", NULL);

            /* success or failure is ignored by the upper layer here.
               it will call GET_CURRENT_CALLS and determine success that way */
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;

        case RIL_REQUEST_SEPARATE_CONNECTION:
            {
                char  cmd[12];
                int   party = ((int*)data)[0];

                // Make sure that party is in a valid range.
                // (Note: The Telephony middle layer imposes a range of 1 to 7.
                // It's sufficient for us to just make sure it's single digit.)
                if (party > 0 && party < 10) {
                    sprintf(cmd, "AT+CHLD=2%d", party);
                    at_send_command(ATch_primary, cmd, NULL);
                    RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
                } else {
                    RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
                }
            }
            break;

        case RIL_REQUEST_SIGNAL_STRENGTH:
            requestSignalStrength(data, datalen, t);
            break;

        case RIL_REQUEST_GET_NEIGHBORING_CELL_IDS:
            requestNeighboaringCellIds(data, datalen, t);
            break;

        case RIL_REQUEST_REGISTRATION_STATE:
        case RIL_REQUEST_GPRS_REGISTRATION_STATE:
            requestRegistrationState(request, data, datalen, t);
            break;

        case RIL_REQUEST_OPERATOR:
            requestOperator(data, datalen, t);
            break;

        case RIL_REQUEST_RADIO_POWER:
            requestRadioPower(data, datalen, t);
            break;

        case RIL_REQUEST_DTMF_START:
        case RIL_REQUEST_DTMF: {
            char c = ((char *)data)[0];
            char *cmd;
            asprintf(&cmd, "AT+VTS=%c", (int)c);
            at_send_command(ATch_primary, cmd, NULL);
            free(cmd);
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        }
        case RIL_REQUEST_DTMF_STOP:
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        case RIL_REQUEST_SEND_SMS:
            requestSendSMS(data, datalen, t);
            break;

        case RIL_REQUEST_SETUP_DATA_CALL:
            requestSetupDefaultPDP(data, datalen, t);
            break;

        case RIL_REQUEST_DEACTIVATE_DATA_CALL:
            /* hang up connection */
            deactivatePDPConnection();
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;

        case RIL_REQUEST_SMS_ACKNOWLEDGE:
            requestSMSAcknowledge(data, datalen, t);
            break;

        case RIL_REQUEST_GET_IMSI:
            p_response = NULL;
            err = at_send_command_numeric(ATch_primary, "AT+CIMI", &p_response);

            if (err < 0 || p_response->success == 0) {
                RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
            } else {
                RIL_onRequestComplete(t, RIL_E_SUCCESS,
                    p_response->p_intermediates->line, sizeof(char *));
            }
            at_response_free(p_response);
            break;

        case RIL_REQUEST_GET_IMEI:
            getIMEInumber(data, datalen, t);
            break;

        case RIL_REQUEST_SIM_IO:
            requestSIM_IO(data,datalen,t);
            break;

        case RIL_REQUEST_SEND_USSD:
            requestSendUSSD(data, datalen, t);
            break;

        case RIL_REQUEST_CANCEL_USSD:
            p_response = NULL;
            err = at_send_command_numeric(ATch_primary, "AT+CUSD=2", &p_response);

            if (err < 0 || p_response->success == 0) {
                RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
            } else {
                RIL_onRequestComplete(t, RIL_E_SUCCESS,
                    p_response->p_intermediates->line, sizeof(char *));
            }
            at_response_free(p_response);
            break;

        case RIL_REQUEST_SET_NETWORK_SELECTION_AUTOMATIC:
            at_send_command(ATch_primary, "AT+CGATT=1", NULL);
            at_send_command(ATch_primary, "AT+COPS=0", NULL);
            at_send_command(ATch_primary, "AT+CGAUTO", NULL);
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;

        case RIL_REQUEST_SET_NETWORK_SELECTION_MANUAL:
            requestNetworkRegistration(data, datalen, t);
            break;

        case RIL_REQUEST_DATA_CALL_LIST:
            requestPDPContextList(data, datalen, t);
            break;

            case RIL_REQUEST_LAST_DATA_CALL_FAIL_CAUSE: {
                int response = PDP_FAIL_ERROR_UNSPECIFIED;
                RIL_onRequestComplete(t, RIL_E_SUCCESS, &response,
                                      sizeof(response));
            }
            break;

        case RIL_REQUEST_QUERY_AVAILABLE_NETWORKS:
            requestNetworkList(data, datalen, t);
            break;

        case RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE:
        {
            int gsmType = 1;
            RIL_onRequestComplete(t, RIL_E_SUCCESS,
                                  &gsmType, sizeof(int *));
        }
        break;

        case RIL_REQUEST_GET_CLIR:
        {
            p_response = NULL;
            int response[2] = {1, 1};

#ifdef HAVE_TI_CALYPSO
            err = at_send_command_singleline(ATch_primary, "AT+CLIR?",
                                             "+CLIR: ", &p_response);
#else
            err = at_send_command_singleline(ATch_gprs, "AT+CLIR?",
                                             "+CLIR: ", &p_response);
#endif
            if (err >= 0 && p_response->success) {
                char *line = p_response->p_intermediates->line;

                err = at_tok_start(&line);

                if (err >= 0) {
                    err = at_tok_nextint(&line, &response[0]);

                    if (err >= 0)
                        err = at_tok_nextint(&line, &response[1]);
                }
            }

            RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));
            at_response_free(p_response);
            break;
        }

        case RIL_REQUEST_SET_CLIR:
        {
            int n = ((int *)data)[0];
            char *cmd;
            asprintf(&cmd, "AT+CLIR=%d", n);
#ifdef HAVE_TI_CALYPSO
            at_send_command(ATch_gprs, cmd, NULL);
#else
            at_send_command(ATch_primary, cmd, NULL);
#endif
            free(cmd);
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        }

        case RIL_REQUEST_QUERY_NETWORK_SELECTION_MODE:
            requestQueryNetworkSelectionMode(data, datalen, t);
            break;

        case RIL_REQUEST_OEM_HOOK_RAW:
            // echo back data
            RIL_onRequestComplete(t, RIL_E_SUCCESS, data, datalen);
            break;

        case RIL_REQUEST_OEM_HOOK_STRINGS: {
            int i;
            const char ** cur;

            LOGD("got OEM_HOOK_STRINGS: 0x%8p %lu", data, (long)datalen);

            for (i = (datalen / sizeof (char *)), cur = (const char **)data ;
                    i > 0 ; cur++, i --) {
                LOGD("> '%s'", *cur);
            }

            // echo back strings
            RIL_onRequestComplete(t, RIL_E_SUCCESS, data, datalen);
            break;
        }

        case RIL_REQUEST_WRITE_SMS_TO_SIM:
            requestWriteSmsToSim(data, datalen, t);
            break;

        case RIL_REQUEST_DELETE_SMS_ON_SIM:
        {
            char * cmd;
            p_response = NULL;
            asprintf(&cmd, "AT+CMGD=%d", ((int *)data)[0]);
            err = at_send_command(ATch_primary, cmd, &p_response);
            free(cmd);
            if (err < 0 || p_response->success == 0) {
                RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
            } else {
                RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            }
            at_response_free(p_response);
            break;
        }

        case RIL_REQUEST_ENTER_SIM_PIN:
        case RIL_REQUEST_ENTER_SIM_PUK:
        case RIL_REQUEST_ENTER_SIM_PIN2:
        case RIL_REQUEST_ENTER_SIM_PUK2:
        case RIL_REQUEST_CHANGE_SIM_PIN:
        case RIL_REQUEST_CHANGE_SIM_PIN2:
            requestEnterSimPin(data, datalen, t);
            break;

        case RIL_REQUEST_SET_MUTE:
        {
            char *cmd;
            asprintf(&cmd, "AT+CMUT=%d", ((int *)data)[0]);
            err = at_send_command(ATch_primary, cmd, NULL);
            free(cmd);
            RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            break;
        }

        case  RIL_REQUEST_GET_MUTE:
        {
            p_response = NULL;
            int response = 0;
            err = at_send_command_singleline(ATch_primary, "AT+CMUT?",
                                             "+CMUT: ", &p_response);
            if (err >= 0 && p_response->success) {
               char *line = p_response->p_intermediates->line;
               err = at_tok_start(&line);
               if (err >= 0) {
                   err = at_tok_nextint(&line, &response);
                   RIL_onRequestComplete(t, RIL_E_SUCCESS, &response,
                                         sizeof(response));
                }
            }
            else
                RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);

            at_response_free(p_response);
            break;
        }

        case RIL_REQUEST_QUERY_CALL_WAITING:
        {
            p_response = NULL;
            int response[2] = {0, 0};
            int c = ((int *)data)[0];
            char *cmd;
            asprintf(&cmd, "AT+CCWA=1,2,%d", c);
            err = at_send_command_singleline(ATch_primary, cmd, "+CCWA: ",
                                             &p_response);
            free(cmd);

            if (err >= 0 && p_response->success) {
                char *line = p_response->p_intermediates->line;

                err = at_tok_start(&line);

                if (err >= 0) {
                    err = at_tok_nextint(&line, &response[0]);

                    if (err >= 0)
                        err = at_tok_nextint(&line, &response[1]);
                }
            }

            RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));
            at_response_free(p_response);
            break;
        }

        case RIL_REQUEST_SET_CALL_WAITING:
        {
            p_response = NULL;
            int enable = ((int *)data)[0];
            int c = ((int *)data)[1];
            char *cmd;
            asprintf(&cmd, "AT+CCWA=1,%d,%d", enable, c);
            err = at_send_command(ATch_primary, cmd,
                                  &p_response);
            free(cmd);
            if (err < 0 || p_response->success == 0) {
                RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
            } else {
                RIL_onRequestComplete(t, RIL_E_SUCCESS, NULL, 0);
            }
            at_response_free(p_response);
            break;
        }

        case RIL_REQUEST_SCREEN_STATE:
            requestScreeState(((int*)data)[0], t);
            break;

        default:
            RIL_onRequestComplete(t, RIL_E_REQUEST_NOT_SUPPORTED, NULL, 0);
            break;
    }

}

/**
 * Synchronous call from the RIL to us to return current radio state.
 * RADIO_STATE_UNAVAILABLE should be the initial state.
 */
static RIL_RadioState
currentState()
{
    return sState;
}
/**
 * Call from RIL to us to find out whether a specific request code
 * is supported by this implementation.
 *
 * Return 1 for "supported" and 0 for "unsupported"
 */

static int
onSupports (int requestCode)
{
    //@@@ todo

    return 1;
}

static void onCancel (RIL_Token t)
{
    //@@@todo

}

static const char * getVersion(void)
{
    return "android freerunner-ril 1.0";
}

static void
setRadioState(RIL_RadioState newState)
{
    RIL_RadioState oldState;

    pthread_mutex_lock(&s_state_mutex);

    oldState = sState;

    if (s_closed > 0) {
        // If we're closed, the only reasonable state is
        // RADIO_STATE_UNAVAILABLE
        // This is here because things on the main thread
        // may attempt to change the radio state after the closed
        // event happened in another thread
        newState = RADIO_STATE_UNAVAILABLE;
    }

    if (sState != newState || s_closed > 0) {
        sState = newState;

        pthread_cond_broadcast (&s_state_cond);
    }

    pthread_mutex_unlock(&s_state_mutex);


    /* do these outside of the mutex */
    if (sState != oldState) {
        RIL_onUnsolicitedResponse (RIL_UNSOL_RESPONSE_RADIO_STATE_CHANGED,
                                    NULL, 0);

        /* FIXME onSimReady() and onRadioPowerOn() cannot be called
         * from the AT reader thread
         * Currently, this doesn't happen, but if that changes then these
         * will need to be dispatched on the request thread
         */
        if (sState == RADIO_STATE_SIM_READY) {
            onSIMReady();
        } else if (sState == RADIO_STATE_SIM_NOT_READY) {
            onRadioPowerOn();
        }
    }
}

/**
 * Get the current card status.
 *
 * This must be freed using freeCardStatus.
 * @return: On success returns RIL_E_SUCCESS
 */
static int getCardStatus(RIL_CardStatus **pp_card_status) {
    static RIL_AppStatus app_status_array[] = {
        // SIM_ABSENT = 0
        { RIL_APPTYPE_UNKNOWN, RIL_APPSTATE_UNKNOWN, RIL_PERSOSUBSTATE_UNKNOWN,
          NULL, NULL, 0, RIL_PINSTATE_UNKNOWN, RIL_PINSTATE_UNKNOWN },
        // SIM_NOT_READY = 1
        { RIL_APPTYPE_SIM, RIL_APPSTATE_DETECTED, RIL_PERSOSUBSTATE_UNKNOWN,
          NULL, NULL, 0, RIL_PINSTATE_UNKNOWN, RIL_PINSTATE_UNKNOWN },
        // SIM_READY = 2
        { RIL_APPTYPE_SIM, RIL_APPSTATE_READY, RIL_PERSOSUBSTATE_READY,
          NULL, NULL, 0, RIL_PINSTATE_UNKNOWN, RIL_PINSTATE_UNKNOWN },
        // SIM_PIN = 3
        { RIL_APPTYPE_SIM, RIL_APPSTATE_PIN, RIL_PERSOSUBSTATE_UNKNOWN,
          NULL, NULL, 0, RIL_PINSTATE_ENABLED_NOT_VERIFIED, RIL_PINSTATE_UNKNOWN },
        // SIM_PUK = 4
        { RIL_APPTYPE_SIM, RIL_APPSTATE_PUK, RIL_PERSOSUBSTATE_UNKNOWN,
          NULL, NULL, 0, RIL_PINSTATE_ENABLED_BLOCKED, RIL_PINSTATE_UNKNOWN },
        // SIM_NETWORK_PERSONALIZATION = 5
        { RIL_APPTYPE_SIM, RIL_APPSTATE_SUBSCRIPTION_PERSO, RIL_PERSOSUBSTATE_SIM_NETWORK,
          NULL, NULL, 0, RIL_PINSTATE_ENABLED_NOT_VERIFIED, RIL_PINSTATE_UNKNOWN }
    };
    RIL_CardState card_state;
    int num_apps;

    SIM_Status sim_status = getSIMStatus();
    if (sim_status == SIM_ABSENT) {
        card_state = RIL_CARDSTATE_ABSENT;
        num_apps = 0;
    } else {
        card_state = RIL_CARDSTATE_PRESENT;
        num_apps = 1;
    }

    // Allocate and initialize base card status.
    RIL_CardStatus *p_card_status = malloc(sizeof(RIL_CardStatus));
    p_card_status->card_state = card_state;
    p_card_status->universal_pin_state = RIL_PINSTATE_UNKNOWN;
    p_card_status->gsm_umts_subscription_app_index = RIL_CARD_MAX_APPS;
    p_card_status->cdma_subscription_app_index = RIL_CARD_MAX_APPS;
    p_card_status->num_applications = num_apps;

    // Initialize application status
    int i;
    for (i = 0; i < RIL_CARD_MAX_APPS; i++) {
        p_card_status->applications[i] = app_status_array[SIM_ABSENT];
    }

    // Pickup the appropriate application status
    // that reflects sim_status for gsm.
    if (num_apps != 0) {
        // Only support one app, gsm
        p_card_status->num_applications = 1;
        p_card_status->gsm_umts_subscription_app_index = 0;

        // Get the correct app status
        p_card_status->applications[0] = app_status_array[sim_status];
    }

    *pp_card_status = p_card_status;
    return RIL_E_SUCCESS;
}

/**
 * Free the card status returned by getCardStatus
 */
static void freeCardStatus(RIL_CardStatus *p_card_status) {
    free(p_card_status);
}

/** returns one of RIM_SIM_*. Returns SIM_NOT_READY on error */
static SIM_Status
getSIMStatus()
{
    ATResponse *p_response = NULL;
    int err;
    int ret = SIM_NOT_READY;
    char *cpinLine;
    char *cpinResult;

    if (sState == RADIO_STATE_OFF || sState == RADIO_STATE_UNAVAILABLE) {
        goto done;
    }

    err = at_send_command_singleline(ATch_primary, "AT+CPIN?", "+CPIN:",
                                     &p_response);

    if (err != 0) {
        ret = SIM_NOT_READY;
        goto done;
    }

    switch (at_get_cme_error(p_response)) {
        case CME_SUCCESS:
            break;

        case CME_SIM_NOT_INSERTED:
            ret = SIM_ABSENT;
            goto done;

        default:
            ret = SIM_NOT_READY;
            goto done;
    }

    /* CPIN? has succeeded, now look at the result */

    cpinLine = p_response->p_intermediates->line;
    err = at_tok_start (&cpinLine);

    if (err < 0) {
        goto done;
    }

    err = at_tok_nextstr(&cpinLine, &cpinResult);

    if (err < 0) {
        ret = SIM_NOT_READY;
        goto done;
    }

    if (0 == strcmp (cpinResult, "SIM PIN")) {
        ret = SIM_PIN;
        goto done;
    } else if (0 == strcmp (cpinResult, "SIM PUK")) {
        ret = SIM_PUK;
        goto done;
    } else if (0 == strcmp (cpinResult, "PH-NET PIN")) {
        return SIM_NETWORK_PERSONALIZATION;
    } else if (0 != strcmp (cpinResult, "READY"))  {
        /* we're treating unsupported lock types as "sim absent" */
        ret = SIM_ABSENT;
        goto done;
    }

    at_response_free(p_response);
    p_response = NULL;
    cpinResult = NULL;

    if (isSMSInit())
        ret = SIM_READY;
    else
        ret = SIM_NOT_READY;

done:
    at_response_free(p_response);
    return ret;
}

/**
 * SIM ready means any commands that access the SIM will work, including:
 *  AT+CPIN, AT+CSMS, AT+CNMI, AT+CRSM
 *  (all SMS-related commands)
 */
static void pollSIMState (void *param)
{
    ATResponse *p_response;
    int ret;

    if (sState != RADIO_STATE_SIM_NOT_READY) {
        // no longer valid to poll
        return;
    }

    switch(getSIMStatus()) {
        case SIM_ABSENT:
        case SIM_PIN:
        case SIM_PUK:
        case SIM_NETWORK_PERSONALIZATION:
        default:
            setRadioState(RADIO_STATE_SIM_LOCKED_OR_ABSENT);
        return;

        case SIM_NOT_READY:
            RIL_requestTimedCallback (pollSIMState, NULL, &TIMEVAL_SIMPOLL);
        return;

        case SIM_READY:
            setRadioState(RADIO_STATE_SIM_READY);
        return;
    }
}

/** returns 1 if on, 0 if off, and -1 on error */
static int isRadioOn()
{
    ATResponse *p_response = NULL;
    int err;
    char *line;
    char ret;

    err = at_send_command_singleline(ATch_primary, "AT+CFUN?", "+CFUN:",
                                     &p_response);

    if (err < 0 || p_response->success == 0) {
        // assume radio is off
        goto error;
    }

    line = p_response->p_intermediates->line;

    err = at_tok_start(&line);
    if (err < 0) goto error;

    err = at_tok_nextbool(&line, &ret);
    if (err < 0) goto error;

    at_response_free(p_response);
    return (int)ret;

error:
    at_response_free(p_response);
    return -1;
}

/**
 * Initialize everything that can be configured while we're still in
 * AT+CFUN=0
 */
static void initializeCallback(void *param)
{
    ATResponse *p_response = NULL;
    int err;

    setRadioState (RADIO_STATE_OFF);

    at_handshake(ATch_primary);
    if (ATch_gprs) {
        /*  Network registration events */
        at_handshake(ATch_gprs);
        err = at_send_command(ATch_gprs, "ATZ", &p_response);
        at_response_free(p_response);
        LOGD("gprs channel init");
    }

    /* note: we don't check errors here. Everything important will
       be handled in onATTimeout and onATReaderClosed */

    /*  Network registration events */
    err = at_send_command(ATch_primary, "ATZ", &p_response);
    at_response_free(p_response);

    /*  atchannel is tolerant of echo but it must */
    /*  have verbose result codes */
    at_send_command(ATch_primary, "ATE0Q0V1", NULL);

   /* use CRING instead of RING */
    at_send_command(ATch_primary, "AT+CRC=1", NULL);

    /* use +CLIP: to indicate CLIP */
    at_send_command(ATch_primary, "AT+CLIP=1", NULL);

    /* use +COLP: to indicate COLP */
    /* set it 0 to disable subscriber info and avoid cme err 512 */
    at_send_command(ATch_primary, "AT+COLP=0", NULL);

    /* use +CCWA: to indicate waiting call */
    at_send_command(ATch_primary, "AT+CCWA=1,1", NULL);

    /* use +CTZU: timezone update */
    at_send_command(ATch_primary, "AT+CTZU=1", NULL);

    /* use +CTZR: timezone reporting */
    at_send_command(ATch_primary, "AT+CTZR=1", NULL);

    /* configure message format as PDU mode*/
    /* FIXME: TEXT mode support!! */
    at_send_command(ATch_primary, "AT+CMGF=0", NULL);

    /*  No auto-answer */
    at_send_command(ATch_primary, "ATS0=0", NULL);

    /*  Extended errors */
    at_send_command(ATch_primary, "AT+CMEE=1", NULL);

    /*  Network registration events */
    err = at_send_command(ATch_primary, "AT+CREG=2", &p_response);

    /* some handsets -- in tethered mode -- don't support CREG=2 */
    if (err < 0 || p_response->success == 0) {
        at_send_command(ATch_primary, "AT+CREG=1", NULL);
    }

    at_response_free(p_response);

    /*  GPRS registration events */
    at_send_command(ATch_primary, "AT+CGREG=2", NULL);

    /*  Call Waiting notifications */
    at_send_command(ATch_primary, "AT+CCWA=1", NULL);

    /*  Alternating voice/data off */
    at_send_command(ATch_primary, "AT+CMOD=0", NULL);

    /*  Not muted */
    at_send_command(ATch_primary, "AT+CMUT=0", NULL);

    /*  +CSSU unsolicited supp service notifications */
    at_send_command(ATch_primary, "AT+CSSN=0,1", NULL);

    /*  HEX character set */
    at_send_command(ATch_primary, "AT+CSCS=\"GSM\"", NULL);

    /*  USSD unsolicited */
    at_send_command(ATch_primary, "AT+CUSD=1", NULL);

    /*  Enable +CGEV GPRS event notifications, but don't buffer */
    at_send_command(ATch_primary, "AT+CGEREP=2,1", NULL);

#ifdef USE_TI_COMMANDS

    at_send_command(ATch_primary, "AT%CPI=3", NULL);
    at_send_command(ATch_primary, "AT%CSCN=1,2,1,2", NULL);
    at_send_command(ATch_primary, "AT%CNIV=1", NULL);
    at_send_command(ATch_primary, "AT%CSQ=1", NULL);

    /*  TI specific -- notifications when SMS is ready (currently ignored) */
    at_send_command(ATch_primary, "AT%CSTAT=1", NULL);
    /* reset simReady status */
    simReady = 0;

#endif /* USE_TI_COMMANDS */

    /* Enable SLEEP */
    set_sleep_mode();

#if 0
    NetInfo.deep_mode = 1;
    getNow(&NetInfo.last_time);
#endif

    /* use @AUL: to load audio table */
    at_send_command(ATch_primary, "AT@AUL=\"0\"", NULL);
    /* use @ST: configure the sidetone level */
    at_send_command(ATch_primary, "AT@ST=\"-26\"", NULL);

    /* assume radio is off on error */
    if (isRadioOn() > 0) {
        setRadioState (RADIO_STATE_SIM_NOT_READY);
    }
}

static void waitForClose()
{
    pthread_mutex_lock(&s_state_mutex);

    while (s_closed == 0) {
        pthread_cond_wait(&s_state_cond, &s_state_mutex);
    }

    pthread_mutex_unlock(&s_state_mutex);
}

/**
 * Called by atchannel when an unsolicited line appears
 * This is called on atchannel's reader thread. AT commands may
 * not be issued here
 */
static void onUnsolicited (const char *s, const char *sms_pdu)
{
    char *line = NULL;
    int err;

    /* Ignore unsolicited responses until we're initialized.
     * This is OK because the RIL library will poll for initial state
     */
    if (sState == RADIO_STATE_UNAVAILABLE) {
        return;
    }

#ifdef FAKE_HAVE_TI_CALYPSO
    /* Reset the deep_mode */
    if (!NetInfo.deep_mode) {
        struct timeval event_time;
        int diff;
        getNow(&event_time);
        diff = diff_timeval_to_ms(&event_time, &NetInfo.last_time);
        if (diff > (MILLISEC_TO_MINUTE * 30) || diff <= 0) {
            NetInfo.deep_mode = 1;
            getNow(&NetInfo.last_time);
            NetInfo.n_registration = 0;
            RIL_requestTimedCallback(deepModeEnable, NULL, NULL);
        }
    }
#endif
    if (strStartsWith(s, "%CSQ:")) {
        int response[2];
        char *tmp;

        line = strdup(s);
        tmp = line;

        at_tok_start(&tmp);

        err = at_tok_nextint(&tmp, &(response[0]));
        if (err < 0) goto out;

#if 0
        /* Check recamping bug */
        if (response[0] == 99) {
            /* It's a possible recamping BUG #1024 */
            NetInfo.force_check = 1;
        } else
            NetInfo.force_check = 0;
#endif

        err = at_tok_nextint(&tmp, &(response[1]));
        if (err < 0) goto out;

        RIL_onUnsolicitedResponse(
                RIL_UNSOL_SIGNAL_STRENGTH,
                &response, sizeof(response));

    } else if (strStartsWith(s, "%CTZV:")) {
        /* TI specific -- NITZ time */
        char *response;
        char *tmp;

        line = strdup(s);
        tmp = line;

        at_tok_start(&tmp);

        err = at_tok_nextstr(&tmp, &response);

        if (err != 0) {
            LOGE("invalid NITZ line %s\n", s);
        } else {
            RIL_onUnsolicitedResponse (
                RIL_UNSOL_NITZ_TIME_RECEIVED,
                response, strlen(response));
        }
    } else if (strStartsWith(s,"+CRING:")
                || strStartsWith(s,"RING")
                || strStartsWith(s,"NO CARRIER")
                || strStartsWith(s,"+CCWA")
    ) {
        RIL_onUnsolicitedResponse (
            RIL_UNSOL_RESPONSE_CALL_STATE_CHANGED,
            NULL, 0);
#ifdef WORKAROUND_FAKE_CGEV
        RIL_requestTimedCallback (onPDPContextListChanged, NULL, NULL);
#endif /* WORKAROUND_FAKE_CGEV */
    } else if (strStartsWith(s,"+CREG:")
                || strStartsWith(s,"+CGREG:")
    ) {
        RIL_onUnsolicitedResponse (
            RIL_UNSOL_RESPONSE_NETWORK_STATE_CHANGED,
            NULL, 0);
#ifdef WORKAROUND_FAKE_CGEV
        RIL_requestTimedCallback (onPDPContextListChanged, NULL, NULL);
#endif /* WORKAROUND_FAKE_CGEV */
    } else if (strStartsWith(s, "+CMT:")) {
        RIL_onUnsolicitedResponse (
            RIL_UNSOL_RESPONSE_NEW_SMS,
            sms_pdu, strlen(sms_pdu));
    } else if (strStartsWith(s, "+CDS:")) {
        RIL_onUnsolicitedResponse (
            RIL_UNSOL_RESPONSE_NEW_SMS_STATUS_REPORT,
            sms_pdu, strlen(sms_pdu));
    } else if (strStartsWith(s, "+CMGR:")) {
        RIL_onUnsolicitedResponse (
            RIL_UNSOL_RESPONSE_NEW_SMS,
            sms_pdu, strlen(sms_pdu));
    } else if (strStartsWith(s, "+CGEV:")) {
        /* Really, we can ignore NW CLASS and ME CLASS events here,
         * but right now we don't since extranous
         * RIL_UNSOL_DATA_CALL_LIST_CHANGED calls are tolerated
         */
        /* can't issue AT commands here -- call on main thread */
        RIL_requestTimedCallback (onPDPContextListChanged, NULL, NULL);
#ifdef WORKAROUND_FAKE_CGEV
    } else if (strStartsWith(s, "+CME ERROR: 150")) {
        RIL_requestTimedCallback (onPDPContextListChanged, NULL, NULL);
#endif /* WORKAROUND_FAKE_CGEV */
    } else if (strStartsWith(s, "+CMTI:")) {
        /* can't issue AT commands here -- call on main thread */
        int location;
        char *response = NULL;
        char *tmp;

        line = strdup(s);
        tmp = line;
        at_tok_start(&tmp);

        err = at_tok_nextstr(&tmp, &response);
        if (err < 0) {
            LOGD("sms request fail");
            goto out;
        }

	if (strcmp(response, "SM")) {
            LOGD("sms request arrive but it is not a new sms");
            goto out;
        }

        /* Read the memory location of the sms */
        err = at_tok_nextint(&tmp, &location);
        if (err < 0) {
            LOGD("error parse location");
            goto out;
        }

        RIL_requestTimedCallback (receiveSMS, (void *)location, NULL);
    }
    else if (strStartsWith(s, "+CUSD:")) {
        char *response[2] = { NULL, NULL };
        char *tmp;
        char *buf = NULL;

        line = strdup(s);
        tmp = line;
        at_tok_start(&tmp);

        err = at_tok_nextstr(&tmp, &(response[0]));
        if (err < 0) {
            LOGE("Error code not present");
            goto out;
        }

        err = at_tok_nextstr(&tmp, &(response[1]));
	if (err == 0) {
            // Convert the response, which is in the GSM 03.38 [25]
            // default alphabet, to UTF-8
            // Since the GSM alphabet characters contain a character
            // above 2048 (the Euro symbol)
            // the string can theoretically expand by 3/2 in length
            // when converted to UTF-8, so we allocate a new buffer, twice
            // the size of the one holding the hex string.
            int len = strlen(response[1]);
            char extension = '\0';
            buf = malloc(2 * len);

            int i = 0;
            while (i < len) {
                char c = *(response[1] + i);
                buf[i] = '\0';
                if (extension) {
                    utf8cat(buf, extension_char(c));
                    extension = '\0';
                } else if (c == 0x1B) {
                    extension =  c;
                } else {
                    utf8cat(buf, gsm_to_unicode[(unsigned int)c]);
                }
                i++;
            }
            response[1] = buf;
            RIL_onUnsolicitedResponse(
                RIL_UNSOL_ON_USSD,
                &response, sizeof(response));
            free(buf);
	} else {
            RIL_onUnsolicitedResponse(
                RIL_UNSOL_ON_USSD,
                &response[0], sizeof(response[0]));
        }
    }
#ifdef HAVE_TI_CALYPSO
    else if (strStartsWith(s, "%CSTAT:") && !isSMSInit()) {
        char *service;
        int  status;
        char *tmp;

        line = strdup(s);
        tmp = line;
        at_tok_start(&tmp);

        err = at_tok_nextstr(&tmp, &service);
        if (err < 0)
           goto out;

        err = at_tok_nextint(&tmp, &status);
        if (err < 0)
           goto out;

        if (status == 0)
            goto out;

        if (0 == strcmp (service, "SMS")) {
            LOGD("SMS status");
            simReady = CSTAT_SMS | CSTAT_PHB;
        } else if (0 == strcmp (service, "PHB")) {
            LOGD("PHB status");
            simReady |= CSTAT_PHB;
        } else if (0 == strcmp (service, "RDY")) {
            LOGD("RDY status");
            simReady = CSTAT_PHB | CSTAT_SMS;
        }
        if (isSMSInit()) {
            /* can't issue AT commands here -- call on main thread */
            RIL_requestTimedCallback (setSMSstatus, NULL, NULL);
        }
    }
#endif
out:
    free(line);
}

/* Called on command or reader thread */
static void onATReaderClosed()
{
    LOGI("AT channel closed\n");
    at_close(ATch_primary);
    at_close(ATch_gprs);
    stop_reader();
    s_closed = 1;

    setRadioState (RADIO_STATE_UNAVAILABLE);
}

/* Called on command thread */
static void onATTimeout()
{
    LOGI("AT channel timeout; closing\n");
    at_close(ATch_primary);
    at_close(ATch_gprs);
    stop_reader();

    s_closed = 1;

    /* FIXME cause a radio reset here */
    setRadioState (RADIO_STATE_UNAVAILABLE);
}

static void usage(char *s)
{
#ifdef RIL_SHLIB
    fprintf(stderr, "reference-ril requires: -p <tcp port> or -d /dev/tty_device\n");
#else
    fprintf(stderr, "usage: %s [-p <tcp port>] [-d /dev/tty_device]\n", s);
    exit(-1);
#endif
}

static void *
mainLoop(void *param)
{
    int fd, fd_gprs;
    int ret;

    property_get(PROPERTY_GPRS_CHANNEL, gprs_device, "/dev/pts/1");

    AT_DUMP("== ", "entering mainLoop()", -1 );
    at_set_on_reader_closed(onATReaderClosed);
    at_set_on_timeout(onATTimeout);

    for (;;) {
        fd = -1;
        fd_gprs = -1;
        while  (fd < 0) {
            if (s_port > 0) {
                fd = socket_loopback_client(s_port, SOCK_STREAM);
            } else if (s_device_socket) {
                fd = socket_local_client( s_device_path,
                                          ANDROID_SOCKET_NAMESPACE_FILESYSTEM,
                                          SOCK_STREAM );
            } else if (s_device_path != NULL) {
                fd = open (s_device_path, O_RDWR | O_NONBLOCK);
                if ( fd >= 0 ) {
                    /* disable echo on serial ports */
                    struct termios  ios;
                    tcgetattr(fd, &ios);
                    ios.c_lflag = 0;  /* disable ECHO, ICANON, etc... */
                    ios.c_cflag |= CRTSCTS;
                    cfsetispeed(&ios, B115200);
                    cfsetospeed(&ios, B115200);
                    tcflush(fd, TCIOFLUSH);
                    tcsetattr(fd, TCSANOW, &ios );
                }
                fd_gprs = open (gprs_device, O_RDWR | O_NONBLOCK);
                if ( fd_gprs >= 0 ) {
                    /* disable echo on serial ports */
                    struct termios  ios;
                    tcgetattr(fd_gprs, &ios);
                    ios.c_lflag = 0;  /* disable ECHO, ICANON, etc... */
                    ios.c_cflag |= CRTSCTS;
                    cfsetispeed(&ios, B115200);
                    cfsetospeed(&ios, B115200);
                    tcflush(fd_gprs, TCIOFLUSH);
                    tcsetattr(fd_gprs, TCSANOW, &ios );
                }
            }

            if (fd < 0) {
                perror ("opening AT interface. retrying...");
                sleep(10);
                /* never returns */
            }
        }

        s_closed = 0;
        init_channels();
        ATch_primary = at_open(fd, "MUX[primary]", onUnsolicited);
        if (ATch_primary == NULL) {
            LOGE ("AT error on at_open\n");
            return 0;
        }

        ATch_primary->nolog = 0;
        if (fd_gprs > 0)
            ATch_gprs = at_open(fd_gprs, "MUX[gprs]", NULL);

        start_reader();

        RIL_requestTimedCallback(initializeCallback, NULL, &TIMEVAL_0);

        // Give initializeCallback a chance to dispatched, since
        // we don't presently have a cancellation mechanism
        sleep(1);

        waitForClose();
        LOGI("Re-opening after close");
    }
}

#ifdef RIL_SHLIB

pthread_t s_tid_mainloop;

const RIL_RadioFunctions *RIL_Init(const struct RIL_Env *env, int argc, char **argv)
{
    int ret;
    int fd = -1;
    int opt;
    pthread_attr_t attr;

    s_rilenv = env;

    while ( -1 != (opt = getopt(argc, argv, "p:d:s:"))) {
        switch (opt) {
            case 'p':
                s_port = atoi(optarg);
                if (s_port == 0) {
                    usage(argv[0]);
                    return NULL;
                }
                LOGI("Opening loopback port %d\n", s_port);
            break;

            case 'd':
                s_device_path = optarg;
                LOGI("Opening tty device %s\n", s_device_path);
            break;

            case 's':
                s_device_path   = optarg;
                s_device_socket = 1;
                LOGI("Opening socket %s\n", s_device_path);
            break;

            default:
                usage(argv[0]);
                return NULL;
        }
    }

    if (s_port < 0 && s_device_path == NULL) {
        usage(argv[0]);
        return NULL;
    }

    pthread_attr_init (&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    ret = pthread_create(&s_tid_mainloop, &attr, mainLoop, NULL);

    return &s_callbacks;
}
#else /* RIL_SHLIB */
int main (int argc, char **argv)
{
    int ret;
    int fd = -1;
    int opt;

    while ( -1 != (opt = getopt(argc, argv, "p:d:"))) {
        switch (opt) {
            case 'p':
                s_port = atoi(optarg);
                if (s_port == 0) {
                    usage(argv[0]);
                }
                LOGI("Opening loopback port %d\n", s_port);
            break;

            case 'd':
                s_device_path = optarg;
                LOGI("Opening tty device %s\n", s_device_path);
            break;

            case 's':
                s_device_path   = optarg;
                s_device_socket = 1;
                LOGI("Opening socket %s\n", s_device_path);
            break;

            default:
                usage(argv[0]);
        }
    }

    if (s_port < 0 && s_device_path == NULL) {
        usage(argv[0]);
    }

    RIL_register(&s_callbacks);

    mainLoop(NULL);

    return 0;
}

#endif /* RIL_SHLIB */
