/*
 * gsmmux util function
 *
 * (C) 2009 Michael Trimarchi <trimarchimichael@panicking.kicks-ass.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef __UTIL_H__
#define __UTIL_H__

#ifdef DEBUG
#define GSM_LOGD(...)   LOGD(__VA_ARGS__)
#else
#define GSM_LOGD(...)   ((void)0);
#endif

int checksubstr(char *haystack, int length, const char *needle);

int at_send(int fd, char *cmd, int timeout);

#endif
