/*
 * gsmmux util function
 *
 * (C) 2009 Michael Trimarchi <trimarchimichael@panicking.kicks-ass.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#define LOG_TAG "gsm_mux"
#include <cutils/log.h>
#include "util.h"

int checksubstr(char *haystack, int length, const char *needle)
{
	int needlesize = strlen(needle);
	int i;

	for (i = 0; i < length; i++) {
		if (!memcmp(haystack, needle, needlesize))
			return 1;
		haystack++;
	}
	return 0;
}

int at_send(int fd, char *cmd, int timeout)
{
	int ret;
	fd_set rdfs;
	struct timeval tout;
	char buffer[512];

	FD_ZERO(&rdfs);
	FD_SET(fd, &rdfs);
	tout.tv_sec = timeout;
	tout.tv_usec = 0;

	LOGD(">> %s", cmd);

	ret = write(fd, cmd, strlen(cmd));
	/* drain the buffer */
	ioctl(fd, TCSBRK, 1);

	while (select(fd + 1, &rdfs, 0, 0, &tout)) {
		ret = read(fd, buffer, 512);
		if (checksubstr(buffer, ret, "OK")) {
			LOGD("<< OK");
			return 0;
		} else if (checksubstr(buffer, ret, "ERROR")) {
			LOGD("<< ERROR");
			return -1;
		}
	}
	GSM_LOGD("TIMEOUT");

	return -1;

}
