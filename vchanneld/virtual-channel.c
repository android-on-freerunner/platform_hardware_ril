/*
 * gsmmux vchanneld daemon
 *
 * (C) 2009 Michael Trimarchi <michael@panicking.kicks-ass.org>
 *	    This daemon is based on the gsm0710 library
 * (C) 2009 Michael 'Mickey' Lauer <mlauer@vanille-media.de>
 * (C) 2000-2008 TROLLTECH ASA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>
#include <pthread.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <math.h>
#include <time.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include "virtual-channel.h"

#include <telephony/ril.h>
#define LOG_TAG	"gsm_mux"
#include <cutils/log.h>
#include <cutils/properties.h>
#include <cutils/sockets.h>
#include <linux/capability.h>
#include <linux/prctl.h>
#include <private/android_filesystem_config.h>
#include "util.h"

#define PROPERTY_GPRS_CHANNEL		"vchanneld.gprs"
#define PROPERTY_VCHANNEL_STATUS	"vchanneld.status"
#define POWER_GSM_PROPERTY		"vchanneld.power"
#define GSM_POWER "/sys/class/i2c-adapter/i2c-0/0-0073/neo1973-pm-gsm.0/power_on"
#define GSM_RESET "/sys/class/i2c-adapter/i2c-0/0-0073/neo1973-pm-gsm.0/reset"

static unsigned char wakeup_sequence[] =
		{ GSM0710_FRAME_FLAG, GSM0710_FRAME_FLAG, };

static int gsm0710_write(struct virtual_channel *vch, int type,
			 const char *buffer, int len);
static int gsm0710_send_test(struct virtual_channel *vch);
static int start_channel(struct phy_device *phy, struct virtual_channel *ch);
static int freeze_channel(struct phy_device *phy, struct virtual_channel *ch);

static void gsm_set(char *path, int on)
{
	int fd, ret;
	char cmd;

	do {
		fd = open(path, O_RDWR);
	} while (fd < 0 && errno == EINTR);

	if (fd < 0) {
		LOGE("could not open GSM");
		return;
	}

	if (on)
		cmd = '1';
	else
		cmd = '0';

	do {
		ret = write(fd, &cmd, 1);
	} while (ret < 0 && errno == EINTR);

	close(fd);
}

static inline struct virtual_channel *get_channel(struct phy_device *phy,
						    int channel)
{
	struct virtual_channel *vch = phy->vch[channel];

	return vch;
}

static inline int get_channel_fd(struct phy_device *phy, int channel)
{
	struct virtual_channel *vch = phy->vch[channel];

	return vch->fd;
}

static inline int search_channel_fd(struct phy_device *phy, int fd)
{
	int i = 0;

	for (i = 1; i < 63; i++) {
		if ((phy->channels[i / 32]) & (1 << (i % 32))) {
			/* the channel is active */
			if (fd == (phy->vch[i])->fd) {
				GSM_LOGD("channel match = %d", i);
				return i;
			}
		}
	}

	return 0;
}

struct virtual_channel *alloc_channel_internal(struct phy_device *phy)
{
	struct virtual_channel *vch, *head;
	int *free = phy->channels;
	int i;

	if (phy->used_channel > GSM0710_MAX_CHANNEL)
		return 0;

	vch = malloc(sizeof(struct virtual_channel));
	if (!vch)
		return 0;

	if (free[0] == -1)
		free++;
	for (i = 0; i < 32; i++)
		if (!(*free & (1 << i))) {
			*free |= (1 << i);
			phy->vch[i] = vch;
			vch->phy = phy;
			vch->channel = i;
			/* find a free slot */
			GSM_LOGD("channel alloc = %d", i);
			phy->used_channel++;
			break;
		}

	return vch;
}

static int dealloc_channel_internal(struct virtual_channel *vch,
				    struct phy_device *phy)
{
	struct virtual_channel *ptr, *prev = NULL;
	if (!vch)
		return 0;

	if (vch->channel > 31)
		phy->channels[0] &= ~(1 << vch->channel);
	else
		phy->channels[1] &= ~(1 << vch->channel);

	phy->used_channel--;

	return 1;
}

static inline int exclude_special_char(char tmp, char *dst)
{
	if (tmp != GSM0710_FRAME_ADV_FLAG && tmp != GSM0710_FRAME_ADV_ESC) {
		*dst = (char)tmp;
	} else {
		*dst = (char)GSM0710_FRAME_ADV_ESC;
		*(dst++) = (char)(tmp ^ GSM0710_FRAME_ADV_ESC_COPML);
		return 1;
	}
	return 0;
}

static int compute_frame_mux_basic(struct virtual_channel *vch, int type,
				   char *src, int len, char *dst)
{
	int hsize, size;
	int channel = vch->channel;

	dst[0] = (char)GSM0710_FRAME_FLAG;
	dst[1] = (char)((channel << 2) | 0x03);
	dst[2] = (char)type;
	hsize = size = 4;
	if (len <= 127) {
		dst[3] = (char)((len << 1) | 0x01);
	} else {
		dst[3] = (char)(len << 1);
		dst[4] = (char)(len >> 7);
		hsize++;
		size++;
	}

	if (len > 0) {
		memcpy(dst + size, src, len);
		size += len;
	}

	dst[size++] = (char)compute_crc((unsigned char *)(dst + 1), hsize - 1);
	dst[size++] = (char)GSM0710_FRAME_FLAG;

	return size;
}

static int compute_frame_mux_advanced(struct virtual_channel *vch, int type,
				      char *data, int len, char *dst)
{
	int crc, size = 0;
	int channel = vch->channel;
	char temp;

        dst[0] = (char)GSM0710_FRAME_ADV_FLAG;
        dst[1] = (char)((channel << 2) | 0x03);
        dst[2] = (char)type;
        crc = compute_crc((unsigned char *)(dst + 1), 2);
        if (type == GSM0710_FRAME_ADV_FLAG || type == GSM0710_FRAME_ADV_ESC) {
            /* Need to quote the type field now that crc has been computed */
            dst[2] = (char)GSM0710_FRAME_ADV_ESC;
            dst[3] = (char)(type ^ GSM0710_FRAME_ADV_ESC_COPML);
            size = 4;
        } else {
            size = 3;
        }
        while ( len > 0 ) {
            temp = *data++ & 0xFF;
            --len;
            if (temp != GSM0710_FRAME_ADV_FLAG && temp != GSM0710_FRAME_ADV_ESC) {
                dst[size++] = (char)temp;
            } else {
                dst[size++] = (char)GSM0710_FRAME_ADV_ESC;
                dst[size++] = (char)(temp ^ GSM0710_FRAME_ADV_ESC_COPML);
            }
        }
        if (crc != GSM0710_FRAME_ADV_FLAG && crc != GSM0710_FRAME_ADV_ESC) {
            dst[size++] = (char)crc;
        } else {
            dst[size++] = (char)GSM0710_FRAME_ADV_ESC;
            dst[size++] = (char)(crc ^ GSM0710_FRAME_ADV_ESC_COPML);
        }
        dst[size++] = (char)GSM0710_FRAME_ADV_FLAG;

	return size;
}

/* Process an incoming GSM 07.10 packet */
static int gsm0710_packet( struct phy_device *phy, int channel, int type,
                           const char *data, int len )
{
    GSM_LOGD("0710 packet ok: chan %d, type 0x%02X, len %d\n", channel, type, len );
    if (type == GSM0710_TYPE_UIH || type == GSM0710_TYPE_UI) {

        if (channel >= 1 && channel <= GSM0710_MAX_CHANNEL) {
		int ret;
		do {
			ret = write(get_channel_fd(phy, channel), data, len);
		} while (ret < 0 && errno == EINTR);
		GSM_LOGD("writes bytes to ch(%d) %d", channel, len);
		fsync(get_channel_fd(phy, channel));
        } else if (channel == 0) {
            /* An embedded command or response on channel 0 */
            if (len >= 2 && data[0] == (char)GSM0710_STATUS_SET) {
                return gsm0710_packet(phy, channel, GSM0710_STATUS_ACK,
                                      data + 2, len - 2);
            } else if (len >= 2 && data[0] == (char)0xC3) {
                /* Incoming terminate request on server side */
                for (channel = 1; channel <= GSM0710_MAX_CHANNEL; ++channel) {
			GSM_LOGD("closing all channel request");
			/* XXXX to be done */
                }
                return 0;
            } else if (len >= 2 && data[0] ==
		       (char)( GSM0710_CMD_TEST | GSM0710_EA | GSM0710_CR)) {
                /* Test command from other side - send the same bytes back */
                char *resp = (char *)alloca(len);
                memcpy(resp, data, len);
                resp[0] = (char)0x41;   /* Clear the C/R bit in the response */
                gsm0710_write(get_channel(phy, 0), GSM0710_DATA, resp, len);
            }
        }

    } else if (type == GSM0710_STATUS_ACK && channel == 0) {

        /* Status change message */
        if (len >= 2) {
            /* Handle status changes on other channels */
            channel = ((data[0] & 0xFC) >> 2);
            if (channel >= 1 && channel <= GSM0710_MAX_CHANNEL) {
		struct virtual_channel *vch;
		GSM_LOGD("status message on other channel");
/* Remove temp the freeze code */
#if 0
		vch = get_channel(phy, channel);
		if ((data[1] & GSM0710_FC) && !(vch->state & VCH_FREEZE)) {
			vch->state |= VCH_FREEZE;
			LOGD("Request to freeze channel");
			freeze_channel(phy, vch);
		}
		if (vch->state & VCH_FREEZE && !(data[1] & GSM0710_FC)) {
			vch->state &= ~VCH_FREEZE;
			LOGD("Request to restart channel");
			start_channel(phy, vch);
		}
#endif
            }
        }

        /* Send the response to the status change request to ACK it */
        GSM_LOGD("received status line signal, sending response");
        char resp[33];
        if ( len > 31 )
            len = 31;
        resp[0] = (char)GSM0710_STATUS_ACK;
        resp[1] = (char)((len << 1) | 0x01);
        memcpy(resp + 2, data, len);
        gsm0710_write(get_channel(phy, 0), GSM0710_DATA, resp, len + 2);

    } else if (type == (0x3F & 0xEF)) {
	/* Incoming channel open request on server side */
	if (channel >= 1 && channel <= GSM0710_MAX_CHANNEL) {
		struct virtual_channel *vch;
		GSM_LOGD("open channel");
		vch = get_channel(phy, channel);
		if (vch && vch->state == VCH_OPEN)
			GSM_LOGD("... channel is just open!!!");
		if (vch)
			vch->state = VCH_OPEN;
		if (type != GSM0710_TYPE_UA)
			gsm0710_write(vch, GSM0710_TYPE_UA |
					   GSM0710_PF, NULL, 0);
	}
    } else if (type == (0x53 & 0xEF)) {
        GSM_LOGD( "D'OH!!! MODEM REQUESTED CHANNEL CLOSE! WTF? HELP ME!\n" );
    } else if (type == GSM0710_TYPE_UA) {
		struct virtual_channel *vch;
		GSM_LOGD("open channel");
		vch = get_channel(phy, channel);
		if (vch && vch->state == VCH_OPEN)
			GSM_LOGD("... channel is just open!!!");
		if (vch)
			vch->state = VCH_OPEN;
	}
	return 1;
}
static void gsm0710_read(struct phy_device *phy)
{
	int len;
	char *ptr;
	ptr = phy->internal_buffer + phy->bytes;

	do {
		len = read(phy->fd, ptr,
			   sizeof(phy->internal_buffer) - phy->bytes);
        } while (len < 0 && errno == EINTR);

	if (len <= 0)
		return;

	phy->bytes += len;

	int posn = 0;
	int posn2;
	int header_size;
	int channel, type;
	while (posn < phy->bytes) {
		if (phy->internal_buffer[posn] == (char)GSM0710_FRAME_FLAG) {
			
			/* Basic format: skip additional 0xF9 bytes between
			   frames */
			while ((posn + 1) < phy->bytes &&
			       phy->internal_buffer[posn + 1] ==
			       (char)GSM0710_FRAME_FLAG) {
				++posn;
			}

			/* We need at least 4 bytes for the header */
			if ((posn + 4) > phy->bytes)
				break;

			/* The low bit of the second byte should be 1,
			   which indicates a short channel number */
			if ((phy->internal_buffer[posn + 1] & 0x01) == 0) {
				++posn;
				continue;
			}

			/* Get the packet length and validate it */
			len = (phy->internal_buffer[posn + 3] >> 1) & 0x7F;
			if ((phy->internal_buffer[posn + 3] & 0x01) != 0) {
				/* Single-byte length indication */
				header_size = 3;
			} else {
				/* Double-byte length indication */
				if ((posn + 5) > phy->bytes)
					break;
				header_size = 4;
			}
			if ((posn + header_size + 2 + len) > phy->bytes)
				break;

			/* Verify the packet header checksum */
			if (((compute_crc((unsigned char *)
					  (phy->internal_buffer + posn + 1),
					  header_size) ^
			      phy->internal_buffer[posn + len + header_size +
						   1]) & 0xFF)
			    != 0) {
				GSM_LOGD("*** GSM 07.10 checksum check failed ***");
				posn += len + header_size + 2;
				continue;
			}

			/* Get the channel number and packet type from the
			   header */
			channel = (phy->internal_buffer[posn + 1] >> 2) & 0x3F;
			type = phy->internal_buffer[posn + 2] & 0xEF;	/* Strip "PF" bit */

			/* Dispatch data packets to the appropriate channel */
			if (!gsm0710_packet(phy, channel, type,
					    phy->internal_buffer + posn +
					    header_size + 1, len)) {
				/* Session has been terminated */
				phy->bytes = 0;
				return;
			}
			posn += len + header_size + 2;

		} else if (phy->internal_buffer[posn] ==
			   (char)GSM0710_FRAME_ADV_FLAG) {

			while ((posn + 1) < phy->bytes &&
			       phy->internal_buffer[posn + 1] ==
			       (char)GSM0710_FRAME_ADV_FLAG) {
				++posn;
			}

			/* Search for the end of the packet */
			len = posn + 1;
			while (len < phy->bytes &&
			       phy->internal_buffer[len] !=
			       (char)GSM0710_FRAME_ADV_FLAG) {
				++len;
			}
			if (len >= phy->bytes) {
				/* There are insufficient bytes for a packet at present */
				if (posn == 0
				    && len >=
				    (int)sizeof(phy->internal_buffer)) {
					/* The buffer is full and we were unable to find a
					   legitimate packet.  Discard the buffer and restart */
					posn = len;
				}
				break;
			}

			/* Undo control byte quoting in the packet */
			posn2 = 0;
			++posn;
			while (posn < len) {
				if (phy->internal_buffer[posn] ==
				    GSM0710_FRAME_ADV_ESC) {
					++posn;
					if (posn >= len)
						break;
					phy->internal_buffer[posn2++] =
					    (char)(phy->
						   internal_buffer[posn++] ^
						   GSM0710_FRAME_ADV_ESC_COPML);
				} else {
					phy->internal_buffer[posn2++] =
					    phy->internal_buffer[posn++];
				}
			}

			/* Validate the checksum on the packet header */
			if (posn2 >= 3) {
				if (((compute_crc((unsigned char *)phy->internal_buffer, 2) ^
				      phy->internal_buffer[posn2 -
							   1]) & 0xFF) != 0) {
					GSM_LOGD("*** GSM 07.10 advanced checksum "
					     "check failed ***");
					continue;
				}
			} else {
				GSM_LOGD("*** GSM 07.10 advanced packet "
				     "is too small ***");
				continue;
			}

			/* Decode and dispatch the packet */
			channel = (phy->internal_buffer[0] >> 2) & 0x3F;
			type = phy->internal_buffer[1] & 0xEF;	/* Strip "PF" bit */
			if (!gsm0710_packet(phy, channel, type,
					    phy->internal_buffer + 2,
					    posn2 - 3)) {
				/* Session has been terminated */
				phy->bytes = 0;
				return;
			}

		} else {
			++posn;
		}
	}
	if (posn < phy->bytes) {
		memmove(phy->internal_buffer, phy->internal_buffer + posn,
			phy->bytes - posn);
		phy->bytes -= posn;
	} else {
		phy->bytes = 0;
	}

	return;
}

static int gsm0710_write(struct virtual_channel *vch, int type,
			 const char *buffer, int len)
{
	int ret;
	struct phy_device *phy = vch->phy;
    
	GSM_LOGD("0710 packet out: chan %d, type 0x%02X, len %d", vch->channel,
		type, len);

	if (phy->mux_mode == GSM0710_BASIC)
		ret = compute_frame_mux_basic(vch, type, (char *)buffer, len,
					      vch->raw_data);
	else
		ret = compute_frame_mux_advanced(vch, type, (char *)buffer, len,
						 vch->raw_data);

	do {
		ret = write(phy->fd, vch->raw_data, ret);
        } while (ret < 0 && errno == EINTR);

	/* drain the buffer */
	ioctl(phy->fd, TCSBRK, 1);

	GSM_LOGD("0710 packet sended");

	return ret;
}

static int gsm0710_send_test(struct virtual_channel *vch)
{
	char data[] = { '\0', '\0',
			'P', 'i', 'n', 'g', '\r', '\n'};
	data[0] = (char)GSM0710_CMD_TEST | GSM0710_CR | GSM0710_EA;
	data[1] = (char)GSM0710_EA | ( 6 << 1 );
	gsm0710_write(vch, GSM0710_DATA, data, sizeof(data));
	return 0;
}


static int vch_write_data(struct virtual_channel *vch, char *ptr, int len)
{
	int ret = 0;
	struct phy_device *phy = vch->phy;

	while ((len - phy->max_length) > 0) {
		ret = gsm0710_write(vch, GSM0710_DATA, ptr, phy->max_length);
		if (ret < 0) {
			LOGE("Unable to write in the virtual channel");
			goto out;
		}

		ptr += phy->max_length;
		len -= phy->max_length;
	}

	ret = gsm0710_write(vch, GSM0710_DATA, ptr, len);
out:
	return ret;
}

int open_phy_device(struct phy_device *phy, char *serial)
{
	int i;
	int status = TIOCM_DTR | TIOCM_RTS;
	struct termios attr;
	int ret;
	int epoll_fd = epoll_create(GSM0710_MAX_CHANNEL + 1);
	char gsm_power[PROPERTY_VALUE_MAX];
	char gsm_reset[PROPERTY_VALUE_MAX];

	property_get(POWER_GSM_PROPERTY, gsm_reset, GSM_RESET);
	property_get(POWER_GSM_PROPERTY, gsm_power, GSM_POWER);

	/* switch off sequence */
	gsm_set(gsm_power, 0);
	sleep(1);
	gsm_set(gsm_reset, 0);
	sleep(1);
	/* switch on sequence */
	gsm_set(gsm_power, 1);
	sleep(1);
	gsm_set(gsm_reset, 1);
	sleep(1);
	gsm_set(gsm_reset, 0);
	sleep(4);

	phy->fd = open(serial, O_RDWR | O_NOCTTY | O_NONBLOCK);
	phy->epoll_fd = epoll_fd;

	GSM_LOGD("open phy device %d", phy->fd);

	tcgetattr(phy->fd, &attr);
	attr.c_cflag &= ~(CSIZE | CSTOPB | PARENB | PARODD);
	attr.c_cflag |= CREAD | CLOCAL | CS8 | CRTSCTS;
	attr.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG);
	attr.c_iflag &= ~(INPCK | IGNPAR | PARMRK | ISTRIP | IXANY | ICRNL);
	attr.c_iflag &= ~(IXON | IXOFF);
	attr.c_oflag &= ~(OPOST | OCRNL);
	attr.c_cc[VMIN] = 0;
	attr.c_cc[VINTR] = 0;
	attr.c_cc[VQUIT] = 0;
	attr.c_cc[VSTART] = 0;
	attr.c_cc[VSTOP] = 0;
	attr.c_cc[VSUSP] = 0;
	cfsetispeed(&attr, phy->port_speed);
	cfsetospeed(&attr, phy->port_speed);
	tcsetattr(phy->fd, TCSANOW, &attr);
	ioctl(phy->fd, TIOCMBIS, &status);
	tcflush(phy->fd, TCIOFLUSH);
	phy->active = 0;

	return 0;
}

static int freeze_channel(struct phy_device *phy, struct virtual_channel *ch)
{
	int ret;
	do {
		ret = epoll_ctl(phy->epoll_fd, EPOLL_CTL_DEL, ch->fd, NULL);
	} while (ret < 0 && errno == EINTR);

	if (!ret) phy->active--;

	return ret;
}

static int start_channel(struct phy_device *phy, struct virtual_channel *ch)
{
	int ret;
	struct epoll_event ev;
	ev.events = EPOLLIN | EPOLLERR | EPOLLHUP;
	ev.data.fd = ch->fd;
	do {
		ret = epoll_ctl(phy->epoll_fd, EPOLL_CTL_ADD, ch->fd, &ev);
	} while (ret < 0 && errno == EINTR);

	if (!ret) phy->active++;

	return ret;
}

struct virtual_channel *open_channel(struct phy_device *phy)
{
	struct virtual_channel *ch = NULL;
	struct termios attr;
	char *pts;
	int fd;
	int ret = 0;
	char *raw_data = (char *)malloc(phy->max_length * 2 + 8);

	ch = alloc_channel_internal(phy);
	if (ch == NULL) {
		GSM_LOGD("failed to allocate channel");
		return NULL;
	}

	if (ch->channel > 0) {
		ch->fd = open("/dev/ptmx", O_RDWR | O_NONBLOCK);
		GSM_LOGD("open ptmx %d", ch->fd);
		ch->ptsname = strdup(ptsname(ch->fd));
		GSM_LOGD("Allocate pseudo terminal %s", ch->ptsname);
		tcgetattr(ch->fd, &attr);
		attr.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
		attr.c_iflag &= ~(INLCR | ICRNL | IGNCR);
		attr.c_oflag &= ~(OPOST | OLCUC | ONLRET | ONOCR | OCRNL);
		tcsetattr(ch->fd, TCSANOW, &attr);
		grantpt(ch->fd);
		unlockpt(ch->fd);
		ch->dup_fd = dup(ch->fd);
		chown(ch->ptsname, AID_RADIO, 0);
	} else {
		ch->fd = phy->fd;
	}

	ret = start_channel(phy, ch);
	ch->raw_data = raw_data;
	ch->write = vch_write_data;

	if (ret < 0)
		return NULL;

	return ch;
};

int init_channel(struct virtual_channel *vch)
{
	gsm0710_write(vch, GSM0710_OPEN, 0, 0);
	vch->state = VCH_WAIT_OPEN;
	return 1;
}

int deinit_channel(struct virtual_channel *vch)
{
	if (vch->channel <= 0 || vch->channel > GSM0710_MAX_CHANNEL)
		return 0;
	gsm0710_write(vch, GSM0710_CLOSE, 0, 0);
	vch->state = VCH_CLOSE;
	return 1;
}


int close_channel(struct virtual_channel *vch)
{
	if (vch->channel <= 0 || vch->channel > GSM0710_MAX_CHANNEL)
		return 0;

	gsm0710_write(vch, GSM0710_CLOSE, 0, 0);
	free(vch->raw_data);
	return 1;
}

int get_speed_index(int speed)
{

	switch (speed) {
	case B115200:
		return 5;
	default:
		return 5;
	}
}

int start_muxer_on_phy(struct phy_device *phy)
{
	char *cmd;
	int ret = 0;

        /* Calypso goes to sleep on its on. If you had no communication
         * with it for 7 sec or more, you should somehow wake it up first,
         * e.g. by sending \x1a and waiting for 0.2 sec */
        write(phy->fd, "\x1a", 1);
        usleep(200000);
	ret = at_send(phy->fd, "\r\n\r\n\r\nAT\r\n", 2);
	if (ret < 0) {
		deinit_channel(get_channel(phy, 0));
		ret = at_send(phy->fd, "AT\r\n", 2);
	}
	ret |= at_send(phy->fd, "ATZ\r\n", 2);
	ret |= at_send(phy->fd, "ATE0\r\n", 2);
	ret |= at_send(phy->fd, "AT+CFUN=0\r\n", 2);
	asprintf(&cmd, "AT+CMUX=%d,%d,%d,%d\r\n",
		 phy->mux_mode, phy->mux_subset,
		 get_speed_index(phy->port_speed), phy->max_length);

	ret |= at_send(phy->fd, cmd, 1);
	free(cmd);
	if (ret < 0)
		return -1;

	return 0;
}

int start_rild(struct virtual_channel *vch) {
	char *cmd;
	LOGD("...starting rild daemon");
	asprintf(&cmd, "-d %s", vch->ptsname);
	if (property_set("rild.libargs", cmd) < 0)
		LOGD("Failed to set rild libargs");
	if (property_set(PROPERTY_VCHANNEL_STATUS, "start") < 0)
		LOGD("Failed to start rild ");
	free(cmd);
	return 0;
}

int start_gprs(struct virtual_channel *vch) {
	char *cmd;
	asprintf(&cmd, "%s", vch->ptsname);
	property_set(PROPERTY_GPRS_CHANNEL, cmd);
        free(cmd);
	return 0;
}

int main(int argc, char **argv)
{
	char *device = "/dev/s3c2410_serial0";
	int timeout = 3000;
	struct phy_device phy;
	int max_retry = 5;
	int i;
	int flags;
#define MAX_ACTIVE 3
	struct virtual_channel *vch[MAX_ACTIVE];
	int ne, ret;
	char *cmd;
	struct epoll_event events[MAX_ACTIVE];
	int use_poll = 0;
#ifdef WATCHDOG
	int watchdog_timeout = 1000;
#endif

	phy.port_speed = B115200;
	phy.used_channel = 0;
	phy.mux_mode = GSM0710_ADVANCED;
	phy.mux_subset = 0;
	phy.max_length = GSM0710_DEFAULT_FRAME_SIZE;
	phy.bytes = 0;

	open_phy_device(&phy, device);
	vch[0] = open_channel(&phy);
	vch[1] = open_channel(&phy);
	vch[2] = open_channel(&phy);
	LOGD("Active channels %d", phy.active);
	GSM_LOGD("Open channels");
	alarm(12);
	if (start_muxer_on_phy(&phy)) {
		LOGD("Error on start muxer or alarm raised\n");
		exit(-1);
	}
	alarm(0);

	flags = fcntl(phy.fd, F_GETFL);
	if (flags == -1) {
		LOGD("F_GETFL failed");
		exit(-1);
	}
	fcntl(phy.fd, F_SETFL, flags & ~O_NONBLOCK);

	init_channel(vch[0]); /* control channel */
	init_channel(vch[1]); /* rild channel */
	init_channel(vch[2]); /* gprs channel */

	vch[1]->cmd = start_rild;
	vch[2]->cmd = start_gprs;

	/* drain serial buffer */
	ioctl(phy.fd, TCSBRK, 1);

	for (;;) {
		int nevents = epoll_wait(phy.epoll_fd, events, phy.active, timeout);
		if (nevents < 0) {
			if (errno != EINTR)
				LOGE("epoll_wait() fail: unespected error: %s",
				     strerror(errno));
			continue;
		}
		if (nevents == 0 && use_poll == 0) {
			int i = 0;
			int allactive = 1;

			for (i = 0; i < phy.active; i++) {
				if (vch[i]->state != VCH_OPEN) {
					init_channel(vch[i]);
					allactive = 0;
				}
			}
			if (allactive) {
				LOGD("channel(s) open %d", phy.active);
				/* rise channel command */
				for (i = 0; i < phy.active; i++)
					if (vch[i]->cmd)
						vch[i]->cmd(vch[i]);
#ifdef WATCHDOG
				timeout = watchdog_timeout;
				use_poll = 1;
#else
				timeout = -1;
#endif

				continue;
			}
			/* drain serial buffer */
			ioctl(phy.fd, TCSBRK, 1);
			if (max_retry-- <= 0) {
				max_retry = 5;
				LOGD("Fail to activate vchanneld use"
				     " serial device... retry");
				for (i = 0; i < phy.active; i++) {
					deinit_channel(vch[i]);
				}
				sleep(1);
				continue;
			}
#ifdef WATCHDOG
		} else if (nevents == 0 && use_poll == 1) {
			/* send poll test command */
			gsm0710_send_test(get_channel(&phy, 0));
#endif
		}

		for (ne = 0; ne < nevents; ne++) {
			if ((events[ne].events & (EPOLLERR | EPOLLHUP)) != 0) {
				LOGE("EPOLLERR after epoll_wait() !?");
				/* close the virtual channel ???*/
				if (events[ne].data.fd == vch[0]->fd) {
					LOGD("control channel trouble");
					goto exit;
				}
			}

			if ((events[ne].events & EPOLLIN) != 0) {
				int fd = events[ne].data.fd;
				int channel = search_channel_fd(&phy, fd);
				char buffer[4096];
				if (channel) {
					struct virtual_channel *ch =
						get_channel(&phy, channel);

					do {
						ret = read(fd, buffer, sizeof(buffer));
					} while (ret < 0 && errno == EINTR);

					if (ret > 0) {
							ch->write(ch,
								  buffer,
								  ret);
					}
				} else if (vch[0]->fd == fd) {
					gsm0710_read(&phy);
				}
			}
		}
	}
exit:
	for (i = phy.active - 1; i >= 0; i--)
		close_channel(vch[i]);
	for (i = 1; i < phy.active; i++) {
		close(vch[i]->fd);
		close(vch[i]->dup_fd);
	}
	close(vch[0]->fd);

	return 0;
}
